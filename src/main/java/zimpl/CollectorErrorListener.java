package zimpl;

import java.util.List;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

/**
 * @since 0.2
 * @author javiermv
 */
public class CollectorErrorListener extends BaseErrorListener {

	protected List<String> errors;

	/**
	 * @param collector Receiving an external collector, instead of
	 * 		creating a new one for each instance, allows for sharing
	 * 		this argument between the Lexer and the Parser, easying
	 * 		error reporting.
	 */
	public CollectorErrorListener(List<String> collector) {
		errors = collector;
	}

	public List<String> getErrors() { return List.copyOf(errors); }

	public void syntaxError(Recognizer<?, ?> r, Object offendingSymbol, 
		int line, int charPositionInLine, String msg, RecognitionException e)
	{
		// from org.antlr.v4.runtime.ConsoleErrorListener
		// example: "line 1:0 mismatched input '<EOF>' expecting {'(', INT, ID}"
		String entry = "line " + line + ":" + charPositionInLine + " " + msg;
		// TODO: in case of Token conflict, see if offendingSymbol & 'e' offer
		// additional information about the involved token types.
		errors.add(entry);
	}

}
