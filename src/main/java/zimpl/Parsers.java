package zimpl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ConsoleErrorListener;
import org.antlr.v4.runtime.DiagnosticErrorListener;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.tree.ParseTree;

/** This class holds static methods that operate on Parser. */
public class Parsers {
	
	public static List<String> getErrors(Parser parser) {
		List<String> result = new ArrayList<>();
		List<? extends ANTLRErrorListener> lnrs = 
				parser.getErrorListeners();
		for (ANTLRErrorListener lnr : lnrs) {
			switch (lnr) {
				case CollectorErrorListener cel -> 
					result.addAll(cel.getErrors());
				case DiagnosticErrorListener del -> { } 
				case ConsoleErrorListener cel -> { }
				default -> {
					String msg = "Unexpected Listener class: " + 
							lnr.getClass();
					throw new IllegalArgumentException(msg);
					}
			}
		}
		return result;
	}
	
	public static ParseTree applyRule(Parser p, int rule) {
		String rulename = p.getRuleNames()[rule];
		try {
			// see https://www.graalvm.org/22.0/reference-manual/native-image/Reflection
			// see https://www.graalvm.org/22.0/reference-manual/native-image/BuildConfiguration/#embedding-a-configuration-file
			Method m = p.getClass().getMethod(rulename);
			Object result = m.invoke(p);
			assert result instanceof ParseTree;
			return ((ParseTree) result);
		} catch (Exception e) {
			// NOTE: wraps NoSuchMethodEx & InvocationTargetEx to avoid
			// polluting method's signatures up the stack
			throw new RuntimeException(e);
		}
	}
	
}
