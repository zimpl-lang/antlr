package zimpl;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.DiagnosticErrorListener;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import de.zib.zimpl.antlr.FormulationLexer;
import de.zib.zimpl.antlr.FormulationParser;

/** 
 * This class adds convenience methods to a FormulationParser.
 * 
 * @since 0.4
 * @author javiermv
 */
public class ZimplParser extends FormulationParser {
	
	public static ZimplParser fromFilename(String filename)
	throws IOException {
		try {
			CharStream input = CharStreams.fromFileName(filename);
			return fromCharStream(input);
		} catch (NoSuchFileException e) {
			String absFilename = Paths.get(filename).normalize()
					.toAbsolutePath().toString();
			IOException d = new NoSuchFileException(absFilename);
			d.initCause(e);
			throw d;
		}
	}
	
	public static ZimplParser fromString(String text) {
		// create a CharStream that reads from the String parameter
		CharStream input = CharStreams.fromString(text);
		return fromCharStream(input);
	}
	
	public static ZimplParser fromCharStream(CharStream input) {
		return fromCharStream(input, false);
	}
	
	static ZimplParser 
	fromCharStream(CharStream input, boolean attachDiagnostic) {
		// create a lexer that feeds off of input CharStream
		FormulationLexer lexer = new FormulationLexer(input);
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		// create a parser that feeds off the tokens buffer
		ZimplParser result = new ZimplParser(tokens);
		setupErrorListenersFor(List.of(lexer, result), attachDiagnostic);
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	public static void setupErrorListenersFor(List<Recognizer> rs,
			boolean attachDiagnostic) 
	{
		List<String> errorsCollector = new ArrayList<>();
		for (Recognizer<?, ?> it : rs) {
			// removes default ConsoleErrorListener
			it.removeErrorListeners();
			// attach diagnostic ErrorListeners
			ANTLRErrorListener lstr = 
					new CollectorErrorListener(errorsCollector);
			it.addErrorListener(lstr);
			if (attachDiagnostic)
				it.addErrorListener(new DiagnosticErrorListener(true));
		}
		return; // unnecessary
	}
	
	protected ZimplParser(TokenStream input) { super(input); }
	
	public ParseTree parse(boolean errorFree) {
		ParseTree result = super.parse();
		List<String> errors = this.getErrors();
		if (errorFree && !errors.isEmpty())
			throw new RuntimeException(errors.toString());
		return result;
	}
	
	public ParseTree applyRule(int rule) {
		return Parsers.applyRule(this, rule);
	}
	
	public List<String> getErrors() { return Parsers.getErrors(this); }
	
}
