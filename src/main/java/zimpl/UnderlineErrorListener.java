package zimpl;

import java.util.ArrayList;

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;

/**
 * @see Definitive ANTLR 4 Reference, Section 9.2, p156
 * @author Terence Parr
 * @author Javier MV
 * @since 0.4
 */
public class UnderlineErrorListener extends CollectorErrorListener {
	
	public UnderlineErrorListener() { super(new ArrayList<>()); }

	public void syntaxError(Recognizer<?, ?> recognizer,
			Object offendingSymbol, int line, int charPositionInLine,
			String msg, RecognitionException e)
	{
		String entry = "line " + line + ": syntax error, " +
				msg + "\n";
		entry += underlineError(recognizer, (Token) offendingSymbol,
				line, charPositionInLine);
		errors.add(entry);
	}
	
	protected String underlineError(Recognizer<?, ?> recognizer,
			Token offendingToken, int line, int charPositionInLine)
	{
		CommonTokenStream tokens =
				(CommonTokenStream) recognizer.getInputStream();
		String input = 
				tokens.getTokenSource().getInputStream().toString();
		String[] lines = input.split("\n");
		String result = lines[line - 1];
		// replaces non-tabs with whitespace
		String underline = result.substring(0, charPositionInLine)
				.replaceAll("[^\t]", " ");
		int start = offendingToken.getStartIndex();
		int stop = offendingToken.getStopIndex();
		if (start >= 0 && stop >= 0)
			underline += "^".repeat(stop-start+1);
		if (!underline.isBlank())
			result += "\n" + underline;
		return result;
	}
	
}
