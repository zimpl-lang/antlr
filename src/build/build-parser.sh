#!/bin/sh
export OUTROOT=src-gen
export PKG=de.zib.zimpl.antlr
export OUTDIR="$OUTROOT"/de/zib/zimpl/antlr
export ANTLR_LIB=antlr-4.13.1-complete.jar
export ANTLR_LIB_PATH=/usr/local/lib/
export CLASSPATH=".:$ANTLR_LIB_PATH$ANTLR_LIB:$CLASSPATH"
export CMD="java org.antlr.v4.Tool -o $OUTDIR -package $PKG -visitor"
rm -rf "$OUTROOT"
if [ ! -f ${ANTLR_LIB_PATH}${ANTLR_LIB} ]; then
    echo "File $ANTLR_LIB not found in $ANTLR_LIB_PATH"	>&2 # echo to stderr
    echo "See lib/download.txt for instructions" >&2
    exit 1
fi
$CMD Expression.g4
$CMD Formulation.g4
