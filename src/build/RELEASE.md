# Releasing instructions


## Steps 
1. check that
    1. there are no open Issues for the Milestone
    2. the Milestone objective has been met
    3. all tests pass
    4. commited build scripts define correct ${version}
    5. META-INF content is updated
    7. README.md is up to date
2. build artifacts[^1]
3. upload built artifacts
    1. remove SNAPSHOT versions for ${version}
    2. commit them to https://gitlab.com/zimpler-tool/bin
    3. copy direct download URL
4. close the current Milestone
5. tag version
    1. name tag: ${version}
    2. from: main
    3. message: "Version ${version}"
6. create release
    1. tag name: ${version}
    2. release title: (blank) [uses tag name as release title]
    3. milestone: ${version}
    4. release notes: rephrased[^2] ${milestone description}
    <!--- TODO: automate adding "Changes" to Release Notes -->
    5. add release assets[^3]
        1. URL: paste copied link from 3.3
        2. title: "${filename} ([${architecture}] ${platform} 'release');
            eg: zimpler-0.4.exe (x64 windows release), dot-adapter-0.1.jar (java release)
6. clean local branches and git refs (merge/pull requests)
7. update ${version}++

[^1]: See [`BUILD.md`](BUILD.md).
[^2]: See previous releases for inspiration.
[^3]: TODO: setup CI/CD procedure with GitLab Generic Package Registry.
