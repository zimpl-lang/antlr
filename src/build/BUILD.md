# Building instructions


## Generate parser files

### Manual creation in linux
In the root project folder, there's the `build-parser.sh` shell script that uses the \*.g4 grammar files to fill the `src-gen` folder with the java source code for the parser.

### Eclipse IDE external builder
- Select Project > Properties > Builders > New > Program.
- In the Main tab, complete Location with the shell script file used for manual generation.
- In the Build Options tab, select "Specify working set...", click on "Specify Resources..." and tick the \*.g4 files.


## Generate jar file `zimpl-parser.jar`

This instructions assume you're using the Eclipse IDE. Adapt them for other IDEs and please contribute that back.

### Automatic generation with the Eclipse IDE
 - Open `build.jardesc` and click "Finish".

### Manual generation with the Eclipse IDE
- Select the project in the Package Explorer.
- In the menu bar, choose File > Export > Java > JAR file.
- Deselect source resources.
- Select the `src-gen` and `src/main/java` folders.
- Fill in "JAR file" with "zimpl-parser-${version}.jar".
- Click "Finish".

**TODO**: autocomplete ${version}

## Create zimpl-parser-sources.zip
**TODO**

## Create zimpl-parser-${version]-javadoc.zip
**TODO**

## Publish grammar files
**TODO**
