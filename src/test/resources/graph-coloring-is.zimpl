# Graph Coloring
# Mehrotra & Trick's Column Generation Formulation
# see https://doi.org/10.1287/ijoc.8.4.344

set V ;		# default graph vertex set name
set E ;		# default graph edge set name

defset max_is(V, E); # (indexed) maximal independent sets of G = (V,E)

# zimpl doesn't allow indexed sets to be assigned a function other
# than 'subsets' or 'powersets', nor to be read from files.
set IS[] := max_is(V, E);
set S := indexset(IS);

var x[S] binary;

# FIXME: remember zimpl requires NAME in objfn
#minimize obj: sum <s> in S : x[s];
minimize: sum <s> in S : x[s];

# FIXME: remember zimpl requires NAME in constraints
#subto cover: forall <i> in V :
subto: forall <i> in V :
    sum <s> in S | <i> in IS[s] : x[s] >= 1;

