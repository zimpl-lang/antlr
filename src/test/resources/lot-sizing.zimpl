# Single Item Lot-Sizing Problem (SILSP)

param N ;

set T := { 1 .. N };
set W := { 0 .. N };

param d[T] ; # demand for <t> in T
param c[T] ; # cost of unit production, for <t> in T
param cs ;   # cost of unit storage
param maxx;  # max production capacity
param maxs;  # max inventory in storage
param mins;  # min inventory in storage
param s0;	 # initial stock

var x[T] >= 0;
var s[W] >= 0;

min cost: sum <t> in T: (c[t] * x[t] + cs * s[t]);

subto defstock: forall <t> in T:
   s[t] == s[t-1] + x[t] - d[t];

subto maxprod: forall <t> in T:
   x[t] <= maxx;

subto maxstock: forall <t> in T:
   s[t] <= maxs;

subto minstock: forall <t> in T:
   s[t] >= mins;

subto initstock: s[0] == s0;
