param C ;
param N ;
set I := { 1..N } ;
param w[I] ;
var x[I] binary ;
maximize : sum <i> in I : w[i]*x[i] ;
subto : sum <i> in I : w[i]*x[i] <= C ;
