import static org.junit.jupiter.api.Assertions.*;
import static util.FormulationTesting.createFormulationParserFor;
import static util.FormulationTesting.getFormulationErrorsFor;
import static util.FormulationTesting.matchSet;
import static util.CommonTesting.*;

import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;
import org.junit.Test;

import de.zib.zimpl.antlr.FormulationParser;

public class FormulationSetTests {
	
	@Test public void setReadDef() {
//		""" set A := { read "f" as "t" } """
		String text = "set A := { read \"f\" as \"t\" }";
		String pattern = "set <ID> := { <readfile> }";
		ParseTreeMatch m = matchSet(text, pattern);
		assertTrue(m.succeeded());
		assertEquals("A", m.get("ID").getText());
		// whitespace removed
		assertEquals("read\"f\"as\"t\"", m.get("readfile").getText());
	}
	
	@Test public void setExprDef() {
		String pattern = "set <ID> := <setExpr>";
		ParseTreeMatch m = matchSet("set A := { }", pattern);
		assertTrue(m.succeeded());
		assertEquals("A", m.get("ID").getText());
		// whitespace removed
		assertEquals("{}", m.get("setExpr").getText());
	}
	
	@Test public void setDecl() {
//		set:'set' sqRef 						# SetDecl
//		|	'set' sqRef ':=' setExpr 			# SetDefExpr
//		|	'set' sqRef ':=' '{' readfile '}' 	# SetDefRead
		ParseTreeMatch m = matchSet("set A", "set <sqRef>");
		assertTrue(m.succeeded());
		assertEquals("A", m.get("sqRef").getText());
	}
	
	@Test public void setMissingKeywordDefinition() {
		FormulationParser parser = createFormulationParserFor("A := {};");
		ParseTree tree = parser.statement();
		// statement : (set | ...) ';'
		// set : 'set' ID ':=' '{' ( λ | literals | tuples )? '}' ;
		String pattern = "<set> ;";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertFalse(m.succeeded());
	}
	
	@Test public void setMissingKeywordDeclaration() {
		FormulationParser parser = createFormulationParserFor("A ;");
		ParseTree tree = parser.statement();
		String pattern = "<set> ;";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertFalse(m.succeeded());
	}
	
	@Test public void setMissingId() {
		List<String> errors = getFormulationErrorsFor("set := {};");
		// set : 'set' ID (':=' '{' λ | csv | ... '}')? ;
		String expected = "line 1:4 no viable alternative at input 'set:='";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setMissingOperator() {
		List<String> errors = getFormulationErrorsFor("set A {};");
		String expected = "line 1:6 mismatched input '{' expecting " +
				"{';', '['}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setMisspelledOperator() {
		List<String> errors = getFormulationErrorsFor("set A = {};");
		List<String> expected = List.of(
				"line 1:6 token recognition error at: '= '",
				"line 1:8 mismatched input '{' expecting {';', '['}"
			);
		assertIterableEquals(expected, errors);
	}
	
	@Test public void setMissingBraceClosingEmpty() {
		List<String> errors = getFormulationErrorsFor("set A := {;");
		String expected = "line 1:10 no viable alternative at input " +
				"'setA:={;'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setMissingBraceClosingIntSingle() {
		List<String> errors = getFormulationErrorsFor("set A := {1;");
		// set : 'set' ID (':=' setDef)? ;
		String expected = "line 1:11 no viable alternative " +
				"at input '{1;'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setMissingBraceClosingIntList() {
//		set:'set' sqRef 						# SetDecl
//		|	'set' sqRef ':=' setExpr 			# SetDefExpr
//		|	'set' sqRef ':=' '{' readfile '}' 	# SetDefRead
		List<String> errors = getFormulationErrorsFor("set A := {1, 2;");
		String expected = "line 1:14 missing '}' at ';'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setMissingBracketOpening() {
		List<String> errors = getFormulationErrorsFor("set C];");
		String expected = "line 1:5 extraneous input ']' expecting ';'";
//		System.err.println(errors);
		assertIterableEquals(List.of(expected), errors);
	}
	
	@Test public void setMissingBracketClosing() {
		List<String> errors = getFormulationErrorsFor("set B[;");
//		System.out.println(errors);
		String expected = // whitespace removed
				"line 1:6 no viable alternative at input 'setB[;'";
		assertIterableEquals(List.of(expected), errors);
	}
	
	@Test public void setMissingID() {
//		set:'set' sqRef 						# SetDecl
//		|	'set' sqRef ':=' setExpr 			# SetDefExpr
//		|	'set' sqRef ':=' '{' readfile '}' 	# SetDefRead
		List<String> errors = getFormulationErrorsFor("set ;");
		String expected = "line 1:4 no viable alternative at input 'set;'";
		assertEquals(List.of(expected), errors);
	}
	
}
