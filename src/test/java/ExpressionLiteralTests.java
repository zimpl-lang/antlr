import static util.ExpressionTesting.*;
import static org.junit.jupiter.api.Assertions.*;
import static util.CommonTesting.*;

import java.util.List;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;

import de.zib.zimpl.antlr.ExpressionParser;

public class ExpressionLiteralTests {
	
	@Test public void int0() {
		// NOTE: this method is unnecessary thorough to keep record of
		// useful ways to detect future error conditions.
		ExpressionParser parser = createExpressionParserFor("0");
		ParseTree tree = parser.nExpr(); // begin parsing at (main) 'nExpr' rule
		String expected = "(nExpr (uExpr (basicExpr 0)))";
		assertEquals(expected, tree.toStringTree(parser));
		assertEquals(0, parser.getNumberOfSyntaxErrors());
		assertEquals(ExpressionParser.NExprContext.class, tree.getClass());
		Token tok = extractToken(tree);
		assertEquals(ExpressionParser.INT, tok.getType());
		assertEquals("0", tok.getText());
		assertTrue(hasConsumedAllInput(parser));
//		basicExpr: INT | ...
		ParseTreeMatch m = matchTree(parser, "<INT>", tree);
		assertTrue(m.succeeded());
		assertEquals("0", m.get("INT").getText());
	}
	
	@Test public void int00() {
		Token t = getParsedToken("00");
		assertEquals(ExpressionParser.INT, t.getType());
		assertEquals("00", t.getText());
	}
	
	@Test public void float0ᵢ() {
		List<String> errors = getExpressionErrorsFor("0.");
		String expected = "line 1:1 token recognition error at: '.'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void floatᵢ0() {
		Token t = getParsedToken(".0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals(".0", t.getText());
	}
	
	@Test public void float0ᵢ0() {
		Token t = getParsedToken("0.0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("0.0", t.getText());
	}
	
	@Test public void float1eᵐ() {
		ExpressionParser parser = createExpressionParserFor("1e-");
		// TokenStream == <INT(1)><ID(e)><???>
		ParseTree t = parser.nExpr(); // begin parsing at 'nExpr' rule
		ParseTreeMatch m = matchTree(parser, "<FLOAT>", t);
		assertFalse(m.succeeded());
		assertFalse(hasConsumedAllInput(parser));
	}
	
	@Test public void float1eᵖ0() {
		Token t = getParsedToken("1e+0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("1e+0", t.getText());
	}
	
	@Test public void floatᵢ0eᵐ() {
		ExpressionParser parser = createExpressionParserFor(".0e-");
		// TokenStream == <FLOAT(.0)><ID(e)><???>
		parser.nExpr(); // begin parsing at 'nEexpr' rule
		assertFalse(hasConsumedAllInput(parser));
	}
	
	@Test public void floatᵢ0eᵖ0() {
		Token t = getParsedToken(".0e+0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals(".0e+0", t.getText());
	}
	
	@Test public void float1ᵢ0eᵖ0() {
		Token t = getParsedToken("1.0e+0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("1.0e+0", t.getText());
	}
	
	@Test public void float1e() {
		ExpressionParser parser = createExpressionParserFor("1e");
		// TokenStream == <INT(1)><ID(e)>
		ParseTree t = parser.nExpr(); // begin parsing at 'nEexpr' rule
		ParseTreeMatch m = matchTree(parser, "<FLOAT>", t);
		assertFalse(m.succeeded());
		assertFalse(hasConsumedAllInput(parser));
	}
	
	@Test public void float1e0() {
		Token t = getParsedToken("1e0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("1e0", t.getText());
	}
	
	@Test public void floatᵢ0e() {
		ExpressionParser parser = createExpressionParserFor(".0e");
		// TokenStream == <FLOAT(.0)><ID(e)>
		parser.nExpr(); // begin parsing at 'nExpr' rule
		assertFalse(hasConsumedAllInput(parser));
	}
	
	@Test public void floatᵢ0e0() {
		Token t = getParsedToken(".0e0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals(".0e0", t.getText());
	}
	
	@Test public void float1ᵢ0e0() {
		Token t = getParsedToken("1.0e0");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("1.0e0", t.getText());
	}
	
	@Test public void floatφ() {
		Token t = getParsedToken("1.6180339887498");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("1.6180339887498", t.getText());
	}
	
	@Test @DisplayName("https://en.wikipedia.org/wiki/Gravitational_constant")
	public void floatG() {
		Token t = getParsedToken("6.674e-11");
		assertEquals(ExpressionParser.FLOAT, t.getType());
		assertEquals("6.674e-11", t.getText());
	}
	
	@Test public void stringEmpty() {
		// NOTE: this method is unnecessary thorough to keep record of
		// useful ways to detect future error conditions.
		String text = "\"\""; // escaped empty string literal ("")
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.expr(); // begin parsing at 'expr' rule
		assertEquals("(expr (strExpr \"\"))", tree.toStringTree(parser));
		assertEquals(0, parser.getNumberOfSyntaxErrors());
		assertEquals(ExpressionParser.ExprContext.class, tree.getClass());
		Token tok = extractToken(tree);
		assertEquals(ExpressionParser.STRING, tok.getType());
		assertEquals(text, tok.getText());
		assertTrue(hasConsumedAllInput(parser));
		// strExpr : STRING | ...;
		ParseTreeMatch m = matchTree(parser, "<STRING>", tree);
		assertTrue(m.succeeded());
		assertEquals(text, m.get("STRING").getText());
	}
	
	@Test public void stringNonEmpty() {
		String text = "\"a\""; // escaped string literal ("a")
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.expr(); // begin parsing at 'expr' rule
		ParseTreeMatch m = matchTree(parser, "<STRING>", tree);
		assertTrue(m.succeeded());
		assertEquals(text, m.get("STRING").getText());
	}
	
	@Test public void comment(){
		String text = "# 10";
		assertEquals("", getChannelText(text, Token.DEFAULT_CHANNEL));
		assertEquals(text, getChannelText(text, Token.HIDDEN_CHANNEL));
	}
	
	@Test public void int0Comment(){
		String text = "0 # 10";
		assertEquals("0", getChannelText(text, Token.DEFAULT_CHANNEL));
		assertEquals("# 10", getChannelText(text, Token.HIDDEN_CHANNEL));
	}
	
}
