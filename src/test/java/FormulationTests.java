import static org.junit.jupiter.api.Assertions.*;
import static util.FormulationTesting.*;
import static util.CommonTesting.asStrings;
import static util.CommonTesting.matchTree;

import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;
import org.junit.Test;

import de.zib.zimpl.antlr.FormulationParser;
import de.zib.zimpl.antlr.FormulationParser.FormulationContext;
import de.zib.zimpl.antlr.FormulationParser.ParseContext;
import util.RawZimplVisitor;
import zimpl.ZimplParser;

public class FormulationTests {
	
	@Test public void knapsack() throws IOException {
		String result = RawZimplVisitor.fromFilename("knapsack.zimpl");
		String expected = """
				param C ;
				param N ;
				set I := { 1 .. N } ;
				param w [ I ] ;
				var x [ I ] binary ;
				maximize : sum < i > in I : w [ i ] * x [ i ] ;
				subto : sum < i > in I : w [ i ] * x [ i ] <= C ;
				<EOF>""";
		assertEquals(expected, result);
	}
	
	@Test public void graphColoringIS() throws IOException {
		String result = 
				RawZimplVisitor.fromFilename("graph-coloring-is.zimpl");
		String expected = """
				set V ;
				set E ;
				defset max_is ( V , E ) ;
				set IS [ ] := max_is ( V , E ) ;
				set S := indexset ( IS ) ;
				var x [ S ] binary ;
				minimize : sum < s > in S : x [ s ] ;
				subto : forall < i > in V : sum < s > in S | < i > in IS [ s ] : x [ s ] >= 1 ;
				<EOF>""";
		assertEquals(expected, result);
	}
	
	@Test public void parseStmtMany() throws IOException {
		ZimplParser parser = ZimplParser.fromFilename("stmt-many.zimpl");
		ParseTree tree = parser.parse(true);
//		parse:	formulation EOF ;
		String expected = "(parse (formulation " +
				"(statement (set set (sqRef A)) ;) " +
				"(statement (variable var (sqRef x)) ;)) <EOF>)";
		assertEquals(expected, tree.toStringTree(parser));
	}
	
	@Test public void parseStmtSingle() throws IOException {
		ZimplParser parser = ZimplParser.fromFilename("stmt-single.zimpl");
		ParseTree tree = parser.parse(true);
		String expectedStrTree = "(parse (formulation " +
				"(statement (set set (sqRef A)) ;)) <EOF>)";
		assertEquals(expectedStrTree, tree.toStringTree(parser));
	}
	
	@Test public void parseExtraneousColon() throws IOException {
		List<String> errors = getParseErrorsFor("colon.zimpl");
		String expected = "line 1:0 extraneous input ':' expecting <EOF>";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void parseEmpty() throws IOException {
		ZimplParser parser = ZimplParser.fromFilename("empty.zimpl");
		ParseTree tree = parser.parse(true);
//		parse:	formulation EOF ;
		assertEquals("[]", tree.toString());
		assertEquals(ParseContext.class, tree.getClass());
		assertEquals("(parse formulation <EOF>)", tree.toStringTree(parser));
	}
	
	@Test public void formulationEmpty() {
		ParseTree tree = parseFormulation("");
		// formulation : constraint*;
		assertEquals(0, tree.getChildCount());
		assertEquals("[]", tree.toStringTree());
		assertEquals(FormulationContext.class, tree.getClass());
	}
	
	// NOTE: within same parser rule, tests are sorted in decreasing
	// complexity order; simpler tests are below more complex ones.
	
	@Test public void parameterReadMany() {
		// """ param m := read "f" as "t" """
		String text = "param m[I] := read \"f\" as \"t\" ";
		String pattern = "param <name:ID>[<index:ID>] := <readfile> ";
		ParseTreeMatch m = matchParameter(text, pattern);
		assertTrue(m.succeeded());
		// whitespace removed
		assertIterableEquals(List.of("m", "I", "read\"f\"as\"t\""),
				asStrings(m, "name", "index", "readfile"));
	}
	
	@Test public void parameterReadSingle() {
		// """ param s := read "f" as "t" """
		String text = "param s := read \"f\" as \"t\" ";
		String pattern = "param <ID> := <readfile> ";
		ParseTreeMatch m = matchParameter(text, pattern);
		assertTrue(m.succeeded());
		// whitespace removed
		assertIterableEquals(List.of("s", "read\"f\"as\"t\""),
				asStrings(m, "ID", "readfile"));
	}
	
	@Test public void parameterAssignedPairMany() {
		String text = "param a[E] := <0> 0.0, <1> 1.0";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.parameter();
		String pattern = "param <name:ID> [<index:ID>]:= <mapping>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "E"),
				asStrings(m, "name", "index"));
		
		String mappingPattern = "<t0:tuple> <e0:expr>, <t1:tuple> <e1:expr>";
		ParseTreeMatch mappingMatch =
				matchTree(parser, mappingPattern, m.get("mapping"));
		assertTrue(mappingMatch.succeeded());
		assertIterableEquals(List.of("<0>", "0.0", "<1>", "1.0"),
				asStrings(mappingMatch, "t0", "e0", "t1", "e1"));
	}
	
	@Test public void parameterAssignedPairSingle() {
		String text = "param a[E] := <0> 0.0";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.parameter();
		// parameter: 'param' sqRef ':=' mapping | ...
		// sqRef	: name=ID ('[' ( csv | index ) ']')?
		// mapping 	: ( duo (',' duo)* )? ;
		// duo 		: tuple expr ;
		String pattern = "param <name:ID> [<index:ID>] := <tuple> <expr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "E", "<0>", "0.0"),
				asStrings(m, "name", "index", "tuple", "expr"));
	}
	
	@Test public void parameterAssignedElem() {
		String text = "param a := 0";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.parameter();
		String pattern = "param <ID> := <expr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "0"),
				asStrings(m, "ID", "expr"));
	}
	
	@Test public void parameterIndexedByCondition() {
		String text = "param a[<i> in {}]";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.parameter();
		// parameter: 'param' sqRef ( ':=' elem )? | ...
		String pattern = "param <sqRef>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertEquals("a[<i>in{}]", m.get("sqRef").getText()); // WS removed
	}
	
	@Test public void parameterIndexedById() {
		String text = "param a[A]";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.parameter();
		// parameter: 'param' sqRef ( ':=' elem )? | ...
		// sqRef	: name=ID ('[' ( csv | index ) ']')?
		String pattern = "param <name:ID> [<set:ID>]";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "A"),
				asStrings(m, "name", "set"));
	}
	
	@Test public void parameterIndexedByNone() {
		FormulationParser parser = createFormulationParserFor("param a");
		ParseTree tree = parser.parameter();
		ParseTreeMatch m = matchTree(parser, "param <ID>", tree);
		assertTrue(m.succeeded());
		assertEquals("a", m.get("ID").getText());
	}
	
	// TODO: add error conditions for parameters, such as misspellings.
	
	@Test public void objectiveNameless() {
		FormulationParser parser =
				createFormulationParserFor("minimize:0");
		ParseTree tree = parser.objective();
		// objective : GOAL name=ID? ':' nExpr;
		// GOAL : 'minimize' | 'maximize' ;
		ParseTreeMatch m = matchTree(parser, "minimize:<nExpr>", tree);
		assertTrue(m.succeeded());
		assertEquals("0", m.get("nExpr").getText());
	}
	
	@Test public void objectiveNamed() {
		FormulationParser parser =
				createFormulationParserFor("minimize z:0");
		ParseTree tree = parser.objective();
		ParseTreeMatch m = matchTree(parser, "minimize <ID>:<nExpr>", tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("z", "0"),
				asStrings(m, "ID", "nExpr"));
	}
	
	@Test public void objectiveMissingColonNameless() {
		List<String> errors = getFormulationErrorsFor("minimize 0");
		String expected = "line 1:9 mismatched input '0' expecting {':', ID}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void objectiveMissingColonNamed() {
		List<String> errors = getFormulationErrorsFor("minimize z 0;");
		String expected = "line 1:11 missing ':' at '0'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void formulationManyConstraints() {
		String text = "subto: 0 <= 1; subto: a >= 0;";
		FormulationParser parser = createFormulationParserFor(text);
		// begin parsing at 'formulation' rule
		ParseTree tree = parser.formulation(); 
		// formulation : statement* ;
		// statement : ( constraint | ... ) ';'
		String pattern = "<s1:statement> <s2:statement>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		// whitespace removed
		assertIterableEquals(List.of("subto:0<=1;", "subto:a>=0;"),
				asStrings(m, "s1", "s2"));
	}
	
	/** Also tests associativity */
	@Test public void constraintForallMany() {
		String text = "subto: forall <i> in I: " + 
				"forall <j> in J: 1 >= i+j";
		// TODO: see if 'forall's can be nested, instead of listed
//		String pattern = "subto: <forall> <comparison>"; // nested alt
		String pattern = "subto: <f1:forall> <f2:forall> <comparison>";
		ParseTreeMatch m = matchConstraint(text, pattern);
		assertTrue(m.succeeded());
		// whitespace removed
//		assertIterableEquals(List.of("forall<i>inI:forall<j>inJ:", "1>=i+j"),
//				asStrings(m, "forall", "comparison")); // nested alt
		assertIterableEquals(
				List.of("forall<i>inI:", "forall<j>inJ:", "1>=i+j"),
				asStrings(m, "f1", "f2", "comparison"));
	}
	
	@Test public void constraintForallMissingCondition() {
		// NOTE: '|' restricts 'condition', it's not the correct
		// separator for 'comparison'.
		String text = "subto: forall <f> in F | f <= 0 ;";
		List<String> errors = getFormulationErrorsFor(text);
		String expected = "line 1:32 mismatched input ';' expecting " +
				"{'do', ':', '+', '-', '*', '/', '<', '>', 'and', " +
				"'xor', 'or', '<=', '!=', '==', '>='}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void constraintForallMisspelledDo() {
		String text = "subto: forall <f> in F od f <= 0 ;";
//		// constraint: 'subto' name=ID? ':' forall comparison ; // nested alt
//		// forall 	 : 'forall' condition 'do' forall | ;		// nested alt
//		constraint	: 'subto' name=ID? ':' forall* comparison ;
//		forall		: 'forall' condition sep=('do' | ':') ;
//		// condition : tuple 'in' setExpr (...)? ;
		List<String> errors = getFormulationErrorsFor(text);
		String expected = "line 1:23 missing {'do', ':'} at 'od'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void constraintForallMisspelledTuple() {
		String text = "subto: forall f in F : f <= 0 ;";
		List<String> errors = getFormulationErrorsFor(text);
		String expected0 = "line 1:14 missing '<' at 'f'";
		String expected1 = "line 1:16 missing '>' at 'in'";
		assertEquals(List.of(expected0, expected1), errors);
	}
	
	@Test public void constraintForallMisspelledForall() {
		String text = "subto: faroll <e> in E do e <= 0 ;";
		List<String> errors = getFormulationErrorsFor(text);
//		String expected0 = "line 1:16 mismatched input '>' expecting ';'";
//		String expected1 = "line 1:26 mismatched input 'e' expecting " +
//				"{'print', 'check', 'forall'}";
//		assertEquals(List.of(expected0, expected1), errors);
		assertEquals(List.of(), errors);
		// TODO: refactor into correct test until some IdChecker (maybe
		// a Listener) looks for redExpr ID in { forall, min, max... }
	}
	
	@Test public void constraintForallSingle() {
		String text = "subto: forall <k> in K : x[k] <= 0";
		// constraint: 'subto' name=ID? ':' forall ;
		// forall : 'forall' condition 'do' forall | comparison ;
		// condition : tuple 'in' setExpr (...)? ;
		String pattern = "subto: forall <condition> : <comparison>";
		ParseTreeMatch m = matchConstraint(text, pattern);
		assertTrue(m.succeeded());
		// whitespace removed
		assertIterableEquals(List.of("<k>inK", "x[k]<=0"),
				asStrings(m, "condition", "comparison"));
	}

	@Test public void constraintNamed() {
		String pattern = "subto <ID> : <comparison>";
		ParseTreeMatch m = matchConstraint("subto c: 0 == 0", pattern);
		assertTrue(m.succeeded());
		// whitespace removed
		assertIterableEquals(List.of("c", "0==0"),
				asStrings(m, "ID", "comparison"));
	}
	
	@Test public void constraintNameless() {
		String pattern = "subto : <comparison>";
		ParseTreeMatch m = matchConstraint("subto: 0 == 0", pattern);
		assertTrue(m.succeeded());
		assertEquals("0==0", m.get("comparison").getText());
	}
	
	@Test public void forall() {
		String text = "forall <i> in I:";
		List<String> errors = getErrorsFor(text, ZimplParser.RULE_forall);
		assertTrue(errors.isEmpty());
		ParseTreeMatch m = matchFormulation(text, "<forall>", ZimplParser.RULE_forall);
		assertTrue(m.succeeded());
		// FIXME: seems to fail if tail recursion produces the empty string
//		line 1:17 no viable alternative at input '<EOF>']
	}
	
	@Test public void functionIdListMany() {
		String text = "defnumb M(a, v)";
//		function: FN_TYPE name=ID '(' csv ')' (':=' xExpr)? ;
		String pattern = "<FN_TYPE> <ID>(<csv>)";
		ParseTreeMatch  m = matchFunction(text, pattern);
		assertTrue(m.succeeded());
		List<String> expected = List.of("defnumb", "M", "a,v");
		assertIterableEquals(expected, 
				asStrings(m, "FN_TYPE", "ID", "csv"));
	}
	
	@Test public void functionIdListSingle() {
		String text = "defnumb M(a)";
		String pattern = "<FN_TYPE> <ID>(<expr>)";
		ParseTreeMatch  m = matchFunction(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("defnumb", "M", "a"),
				asStrings(m, "FN_TYPE", "ID", "expr"));
	}
	
	@Test public void functionIdListEmpty() {
		String text = "defset N()";
		String pattern = "<FN_TYPE> <ID>(<csv>)";
		ParseTreeMatch  m = matchFunction(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("defset", "N", ""),
				asStrings(m, "FN_TYPE", "ID", "csv"));
	}
	
	// TODO: typechecker should check this, not parser
	// FIXME: @Disabled does not seem to work; test is executed anyway
	// FIXME: JavaDoc for @Disabled is not found by Eclipse
//	@Disabled @Test public void functionIdListError() {
	public void functionIdListError() {
		String text = "defnumb N(1);";
		List<String> errors = getFormulationErrorsFor(text);
		String expected = "line 1:12 mismatched input ';' expecting " +
				"{',', ')', '+', '-', '*', '/'}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void functionMisspelledDefnumb() {
		List<String> errors = getFunctionErrorsFor("defnum N(v);");
		String expected0 = "line 1:0 missing FN_TYPE at 'defnum'";
		String expected1 = "line 1:7 extraneous input 'N' expecting '('";
		assertIterableEquals(List.of(expected0, expected1), errors);
	}
	
	@Test public void functionDefSetExpr() {
		String text = "defset N() := {};";
		String pattern = "<FN_TYPE> <ID>(<csv>) := <setExpr>";
		ParseTreeMatch  m = matchFunction(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("defset", "N", "", "{}"),
				asStrings(m, "FN_TYPE", "ID", "csv", "setExpr"));
	}
	
	@Test public void functionDefExpr() {
		String text = "defnumb N() := 1 + 1;";
		String pattern = "<FN_TYPE> <ID>(<csv>) := <nExpr>";
		ParseTreeMatch  m = matchFunction(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("defnumb", "N", "", "1+1"),
				asStrings(m, "FN_TYPE", "ID", "csv", "nExpr"));
	}
	
	@Test public void functionDefBoolExpr() {
		String text = "defbool N() := true;";
		String pattern = "<FN_TYPE> <ID>(<csv>) := <boolExpr>";
		ParseTreeMatch  m = matchFunction(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("defbool", "N", "", "true"),
				asStrings(m, "FN_TYPE", "ID", "csv", "boolExpr"));
	}
	
	@Test public void functionMissingBody() {
		String text = "defnumb N() := ;";
		List<String> errors = getFormulationErrorsFor(text);
		String expected = "line 1:15 mismatched input ';' expecting " +
				"{'{', '+', '-', '(', '<', 'not', 'true', 'false', 'if', " +
				"'infinity', INT, FLOAT, STRING, ID}";
		assertEquals(List.of(expected), errors);
	}
}
