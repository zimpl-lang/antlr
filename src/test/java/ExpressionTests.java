import static de.zib.zimpl.antlr.ExpressionParser.*;
import static org.junit.jupiter.api.Assertions.*;
import static util.ExpressionTesting.*;
import static util.CommonTesting.matchTree;
import static util.CommonTesting.hasConsumedAllInput;
import static util.CommonTesting.asStrings;
import static util.CommonTesting.asString;

import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;
import org.antlr.v4.runtime.tree.pattern.ParseTreePatternMatcher.StartRuleDoesNotConsumeFullPattern;
import org.junit.Test;

import de.zib.zimpl.antlr.ExpressionParser;
import zimpl.ZimplParser;

public class ExpressionTests {
	
	/*
	 * Most tests are expressed in *condensed* form, using as few
	 * lines of code as possible. Some tests are in *expanded* form,
	 * usually to help debug or to highlight something.   
	 */
	
	@Test public void signedNone() {
		List<String> errors = getExpressionErrorsFor("+");
		// WARN: vscode replaces '<>' with '[]' in comparison dialogs
		// from assertion failures; makes problem detection hard. 
		String expected = "line 1:1 mismatched input '<EOF>' expecting " +
				"{'(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void signedSigned0() {
		List<String> errors = getExpressionErrorsFor("++0");
		String expected = "line 1:1 extraneous input '+' expecting " +
				"{'(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void signedInt() {
		ParseTreeMatch m = matchExpression("+1", "+ <basicExpr>");
		assertTrue(m.succeeded());
		assertEquals("1", m.get("basicExpr").getText());
	}
	
	@Test public void signedFloat() {
		ParseTreeMatch m = matchExpression("+.1", "+ <basicExpr>");
		assertTrue(m.succeeded());
		assertEquals(".1", m.get("basicExpr").getText());
	}
	
	@Test public void signedId() {
		ParseTreeMatch m = matchExpression("+a", "+ <basicExpr>");
		assertTrue(m.succeeded());
		assertEquals("a", m.get("basicExpr").getText());
	}
	
	@Test public void signedParentesis() {
		ParseTreeMatch m = matchExpression("+(1)", "+ <basicExpr>");
		assertTrue(m.succeeded());
		assertEquals("(1)", m.get("basicExpr").getText());
	}
	
	@Test public void signedBinaryPlus() {
		ParseTreeMatch m = matchExpression("+1+1", "+ <uExpr>");
		assertTrue(m.succeeded());
		assertEquals("1+1", m.get("uExpr").getText());
	}
	
	@Test public void signedBinaryProduct() {
		ParseTreeMatch m = matchExpression("+1*1", "+ <uExpr>");
		assertTrue(m.succeeded());
		assertEquals("1*1", m.get("uExpr").getText());
	}
	
	@Test public void signedSum() {
		String text = "-sum <i> in {} : i";
//		// sumExpression : 'sum' condition ('do' | ':') expr ;
//		redExpr :	op=('min'|'max'|'prod'|'sum') condition sep=('do'|':') nExpr ;
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.nExpr();
		ParseTreeMatch m = matchTree(parser, "- <redExpr>", tree);
		assertTrue(m.succeeded());
		ParseTree sumTree = m.get("redExpr");
		ParseTreeMatch summ = matchTree(parser, "sum <condition>:<ID>", sumTree);
		assertTrue(summ.succeeded());
		assertEquals("<i>in{}", summ.get("condition").getText());
		assertEquals("i", summ.get("ID").getText());
	}
	
	@Test public void signedDoubleOperator() {
		ExpressionParser parser = createExpressionParserFor("+0*-1");
		ParseTree tree = parser.nExpr();
		assertFalse(hasConsumedAllInput(parser));
		String pattern = "<nExpr> * <nExpr>";
		assertThrows(StartRuleDoesNotConsumeFullPattern.class, () -> {
				matchTree(parser, pattern, tree);
			});
	}
	
	@Test public void additionAssociativity() {
		ExpressionParser parser = createExpressionParserFor("1+2+3");
		ParseTree tree = parser.nExpr();
		String pattern = "<lhs:uExpr> + <rhs:uExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertEquals("1+2", m.get("lhs").getText());
		assertEquals("3", m.get("rhs").getText());
		ParseTree lhs = m.get("lhs");
		ParseTreeMatch lhsMatch = matchTree(parser, pattern, lhs);
		assertTrue(lhsMatch.succeeded());
	}
	
	@Test public void productAssociativity() {
		String pattern = "<lhs:uExpr> * <rhs:uExpr>";
		ParseTreeMatch m = matchExpression("a*b*c", pattern);
		assertTrue(m.succeeded());
		assertEquals("a*b", m.get("lhs").getText());
		assertEquals("c", m.get("rhs").getText());
		String lhs = m.get("lhs").getText();
		ParseTreeMatch lhsMatch = matchExpression(lhs, pattern);
		assertTrue(lhsMatch.succeeded());
	}
	
	@Test public void additionPrecedence() {
		String pattern = "<lhs:uExpr> + <rhs:uExpr>";
		ParseTreeMatch m = matchExpression("1+2*3", pattern);
		assertTrue(m.succeeded());
		assertEquals("1", m.get("lhs").getText());
		assertEquals("2*3", m.get("rhs").getText());
	}
	
	@Test public void unsignedDoubleOperator() {
		ExpressionParser parser = createExpressionParserFor("0*-1");
		ParseTree tree = parser.nExpr();
		assertFalse(hasConsumedAllInput(parser));
		// TokenStream == <INT(0)><???>
		String pattern = "<INT> * <nExpr>";
		assertThrows(StartRuleDoesNotConsumeFullPattern.class, () -> {
				matchTree(parser, pattern, tree);
			});
	}
	
	@Test public void basicExprIdUnindexed() {
		testBasicExpr("i");
		testLabeledBasicExpr("i", "ID");
	}
	
	@Test public void basicExprIdIndexedSingle() {
//		basicExpr: sqRef | ...
//		sqRef	: name=ID ('[' ( csv | index ) ']')? ;
//		csv		:	( elem (',' elem)* )? ;
//		elem	:	tuple | nExpr | strExpr ;
		int rule = ExpressionParser.RULE_basicExpr;
		String pattern = "<ID> [ <csv> ]";
		ParseTreeMatch m = matchExpression("x[E]", pattern, rule);
		assertTrue(m.succeeded());
		assertEquals("x", m.get("ID").getText());
		assertEquals("E", m.get("csv").getText());
	}
	
	@Test public void basicExprInt() {
		// basicExpr : INT | ...
		testBasicExpr("1");
		testLabeledBasicExpr("1", "INT");
	}
	
	@Test public void basicExprFloat() {
		// basicExpr : FLOAT | ...
		testBasicExpr("1.0");
		testLabeledBasicExpr("1.0", "FLOAT");
	}
	
	@Test public void basicExprSum() {
		// basicExpr : sumExpr  | ... ;
		// sumExpr : 'sum' condition ('do' | ':') nExpr ;
		testBasicExpr("sum <i> in {} : i");
	}
	
	@Test public void basicExprParentesis() {
		// basicExpr: '(' nExpr ')' | ...
		testBasicExpr("(0)");
	}
	
	@Test public void setExprCrossProductAssociativity() {
		String pattern = "<lhs:setExpr> * <rhs:setExpr>";
		ParseTreeMatch m = matchSetExpr("A*B*C", pattern);
		assertTrue(m.succeeded());
		String lhs = m.get("lhs").getText();
		assertEquals("A*B", lhs);
		assertEquals("C", m.get("rhs").getText());
		ParseTreeMatch lhsMatch = matchSetExpr(lhs, pattern);
		assertTrue(lhsMatch.succeeded());
	}
	
	@Test public void setExprMix() {
		String pattern = "<lhs:setExpr> * <rhs:setExpr>";
		ParseTreeMatch m = matchSetExpr("A*({0}+B)", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("A", "({0}+B)"),
				asStrings(m, "lhs", "rhs"));
	}
	
	@Test public void setExprDifferenceMinus() {
		String pattern = "( <setExpr> ) - <sqRef>";
		ParseTreeMatch m = matchSetExpr("(D) - E", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("D", "E"),
				asStrings(m, "setExpr", "sqRef"));
	}
	
	@Test public void setExprDifferenceBar() {
//		String pattern = "<lhs:setExpr> \\u005c <rhs:setExpr>";
//		String pattern = "<lhs:sqRef> \\ <rhs:sqRef>"; // FIXME: seems not to be working
//		ParseTreeMatch m = matchSetExpr("H \\ I", pattern); // throws new StartRuleDoesNotConsumeFullPattern();
//		assertTrue(m.succeeded());
//		assertIterableEquals(List.of("H", "I"),
//				asStrings(m, "lhs", "rhs"));
		ZimplParser parser = ZimplParser.fromString("H \\ I");
		ParseTree tree = parser.setExpr();
		assertTrue(parser.getErrors().isEmpty());
		String expected = "(setExpr (setExpr (sqRef H)) \\ " +
				"(setExpr (sqRef I)))";
		assertEquals(expected, tree.toStringTree(parser));
	}
	
	@Test public void setExprUnion() {
		String pattern = "<sqRef> + <fnRef>";
		ParseTreeMatch m = matchSetExpr("B[i] + f()", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("B[i]", "f()"),
				asStrings(m, "sqRef", "fnRef"));
	}
	
	@Test public void setExprCrossProduct() {
//		setExpr : setExpr '*' setExpr | setExpr ('+'|'\\'|'-') setExpr
//				| '(' setExpr ')' | setDesc | fnRef | sqRef;
		String pattern = "<setDesc> * <sqRef>";
		ParseTreeMatch m = matchSetExpr("{} * A", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("{}", "A"),
				asStrings(m, "setDesc", "sqRef"));
	}
	
	@Test public void setExprParen() {
		ParseTreeMatch m = matchSetExpr("({ })", "( <setExpr> )");
		assertTrue(m.succeeded());
		assertEquals("{}", asString(m, "setExpr")); // whitespace removed
	}
	
	@Test public void setExprSetDescEmpty() {
		ParseTreeMatch m = matchSetExpr("{ }", "<setDesc>");
		assertTrue(m.succeeded());
		assertEquals("{}", asString(m, "setDesc")); // whitespace removed
	}
	
	@Test public void setExprFnRefMany() {
		ParseTreeMatch m = matchSetExpr("h(f(),g(1)", "<fnRef>");
		assertTrue(m.succeeded());
		assertEquals("h(f(),g(1)", asString(m, "fnRef"));
	}
	
	@Test public void setExprFnRefSingle() {
		ParseTreeMatch m = matchSetExpr("g(0)", "<fnRef>");
		assertTrue(m.succeeded());
		assertEquals("g(0)", asString(m, "fnRef"));
	}
	
	@Test public void setExprFnRefNone() {
//		setExpr : setExpr '*' setExpr | setExpr ('+'|'\\'|'-') setExpr
//				| '(' setExpr ')' | setDesc | fnRef | sqRef;
		ParseTreeMatch m = matchSetExpr("f()", "<fnRef>");
		assertTrue(m.succeeded());
		assertEquals("f()", asString(m, "fnRef"));
	}
	
	@Test public void setExprSqRefMany() {
		ParseTreeMatch m = matchSetExpr("r[s[i],j]", "<sqRef>");
		assertTrue(m.succeeded());
		assertEquals("r[s[i],j]", asString(m, "sqRef"));
	}
	
	@Test public void setExprSqRefSingle() {
		ParseTreeMatch m = matchSetExpr("q[i]", "<sqRef>");
		assertTrue(m.succeeded());
		assertEquals("q[i]", asString(m, "sqRef"));
	}
	
	@Test public void setExprSqRefNone() {
		ParseTreeMatch m = matchSetExpr("p", "<sqRef>");
		assertTrue(m.succeeded());
		assertEquals("p", asString(m, "sqRef"));
	}
	
	@Test public void setDescEmpty() {
		// setDesc : '{' '}' | ... ;
		int rule = ExpressionParser.RULE_setDesc;
		testLabeledExpr("{ }", rule, "setDesc");
	}
	
	@Test public void setDescCsvSingle() {
		String text = "{\"a\"}"; // {"a"}
		// setDesc : '{' csv '}' | ... ;
		ParseTreeMatch m = matchSetExpr(text, "{ <STRING> }");
		assertTrue(m.succeeded());
		assertEquals("\"a\"", m.get("STRING").getText());
	}
	
	@Test public void setDescCsvMany() {
		ParseTreeMatch m = matchSetExpr("{0, .1}", "{<INT>, <FLOAT>}");
		assertTrue(m.succeeded());
		assertEquals("0", m.get("INT").getText());
		assertEquals(".1", m.get("FLOAT").getText());
	}
	
	// TODO: add other types of expressions in range tests, such as:
	//	-1, 2*3, (a), sum...
	
	@Test public void setDescRange() {
		// setDesc : '{' range '}' ;
		// range : lhs=nExpr '..' rhs=nExpr ;
		ParseTreeMatch m = matchSetExpr("{1..N}", "{<INT>..<ID>}");
		assertTrue(m.succeeded());
		assertEquals("1", m.get("INT").getText());
		assertEquals("N", m.get("ID").getText());
	}
	
	@Test public void setDescTupleSingle() {
		// setDesc : '{' tuple (',' tuple)* '}' | ... ;
		ParseTreeMatch m = matchSetExpr("{ <0> }", "{ <tuple> }");
		assertTrue(m.succeeded());
		assertEquals("<0>", m.get("tuple").getText());
	}
	
	@Test public void setDescTupleMany() {
		String pattern = "{<t1:tuple>, <t2:tuple>}";
		ParseTreeMatch m = matchSetExpr("{<1>, <2>}", pattern);
		assertTrue(m.succeeded());
		assertEquals("<1>", m.get("t1").getText());
		assertEquals("<2>", m.get("t2").getText());
	}
	
	@Test public void setDescMissingBraceOpening() {
		int rule = ExpressionParser.RULE_setDesc;
		List<String> errors = getExpressionErrorsFor("}", rule);
		// setDesc : '{' ( | ... ) '}' ;
		String expected = "line 1:0 mismatched input '}' expecting '{'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setDescMissingBraceClosing() {
		int rule = ExpressionParser.RULE_setDesc;
		List<String> errors = getExpressionErrorsFor("{", rule);
		String expected = "line 1:1 no viable alternative at input '{'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void setDescMissingElementSeparator() {
		int rule = ExpressionParser.RULE_setDesc;
		List<String> errors = getExpressionErrorsFor("{0 1}", rule);
		// setDesc : '{' csv '}' | ...  ;
		String expected = "line 1:3 no viable alternative at input '{01'";
		assertEquals(List.of(expected), errors);
	}
	
	// TODO: add more 'range' tests
	
	@Test public void range() {
//		range	:	lhs=nExpr '..' rhs=nExpr ;
		ParseTreeMatch m = matchExpression("{0..1}", "{ <range> }");
		assertTrue(m.succeeded());
	}
	
	
	@Test public void tupleSingleCsv() {
		// tuple: '<' csv '>' ;
		// csv	:	( elem (',' elem)* )? ;
		// elem	:	tuple | nExpr | strExpr ;
		ParseTreeMatch m = matchTuple("<0>", "\\< <csv> \\>");
		assertTrue(m.succeeded());
		assertEquals("0", m.get("csv").getText());
	}
	
	@Test public void tupleSingleTuple() {
		ParseTreeMatch outer = matchTuple("<<0>>", "<tuple>");
		assertTrue(outer.succeeded());
		assertEquals("<<0>>", outer.get("tuple").getText());
		ParseTreeMatch inner = matchTuple("<<0>>", "\\< <tuple> \\>");
		assertTrue(inner.succeeded());
		assertEquals("<0>", inner.get("tuple").getText());
	}
	
	@Test public void tupleSingleParentesis() {
		ParseTreeMatch m = matchTuple("<(0)>", "<tuple>");
		assertTrue(m.succeeded());
		assertEquals("<(0)>", m.get("tuple").getText());
	}
	
	@Test public void tupleSingleSignedInt() {
		ParseTreeMatch outer = matchTuple("<-1>", "<tuple>");
		assertTrue(outer.succeeded());
		assertEquals("<-1>", outer.get("tuple").getText());
		ParseTreeMatch inner = matchTuple("<-1>", "\\< <nExpr> \\>");
		assertTrue(inner.succeeded());
		assertEquals("-1", inner.get("nExpr").getText());
	}
	
	@Test public void tupleSingleSignedFloat() {
		ParseTreeMatch outer = matchTuple("<+1.0>", "<tuple>");
		assertTrue(outer.succeeded());
		assertEquals("<+1.0>", outer.get("tuple").getText());
		ParseTreeMatch inner = matchTuple("<+1.0>", "\\< <nExpr> \\>");
		assertTrue(inner.succeeded());
		assertEquals("+1.0", inner.get("nExpr").getText());
	}
	
	@Test public void tupleManyUnsigned() {
		String text = "< 0, 1.0, \"a\", id >"; // escaped <0,1.0,"a",id>
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.tuple();
		// tuple: '<' csv '>' ;
		// csv	:	( elem (',' elem)* )? ;
		// elem	:	tuple | nExpr | strExpr ;
		String pattern = "\\< <INT>, <FLOAT>, <STRING>, <ID> \\>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("0", "1.0", "\"a\"", "id"),
				asStrings(m, "INT", "FLOAT", "STRING", "ID"));
	}
	
	@Test public void tupleManySigned() {
		String text = "< -1, +2.0, \"a\", id >"; // escaped <-1,+2.0,"a",id>
		String pattern = "\\< -<INT>, +<FLOAT>, <STRING>, <ID> \\>";
		ParseTreeMatch m = matchTuple(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("1", "2.0", "\"a\"", "id"),
				asStrings(m, "INT", "FLOAT", "STRING", "ID"));
	}
	
	@Test public void sumMinimalDo() {
		String text = "sum <i> in A do i";
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.redExpr();
//		redExpr :	op=('min'|'max'|'prod'|'sum') condition sep=('do'|':') nExpr ;
//		// sumExpr	: 'sum' condition ('do' | ':') nExpr ;
		// condition: tuple 'in' setExpr (('with' | '|') boolExpr)? ;
		String pattern = "sum <tuple> in <setExpr> do <nExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("<i>", "A", "i"),
				asStrings(m, "tuple", "setExpr", "nExpr"));
	}
	
	@Test public void sumMinimalColon() {
		ExpressionParser parser = createExpressionParserFor("sum <j> in B : 0");
		ParseTree tree = parser.redExpr();
		String pattern = "sum <tuple> in <setExpr> : <nExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("<j>", "B", "0"),
				asStrings(m, "tuple", "setExpr", "nExpr"));
	}
	
	@Test public void sumPairs() {
		String text = "sum <i,j> in {<0,0>, <1,1>} : i+j";
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.redExpr();
		String pattern = "sum <tuple> in <setExpr> : <nExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("<i,j>", "{<0,0>,<1,1>}", "i+j"),
				asStrings(m, "tuple", "setExpr", "nExpr")); // WS removed
	}
	
	@Test public void sumAssociativity() {
		String text = "sum <i> in A : sum <j> in B : 0";
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.nExpr();
//		redExpr :	op=('min'|'max'|'prod'|'sum') condition sep=('do'|':') nExpr ;
//		// sumExpr : 'sum' condition ('do' | ':') nExpr ;
		// condition: tuple 'in' setExpr (('with' | '|') boolExpr)? ;
		String pattern = "sum <condition> : <redExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		// whitespace removed
		assertEquals("<i>inA", m.get("condition").getText());
		ParseTree inTree = m.get("redExpr");
		String inPattern = "sum <condition> : <INT>";
		ParseTreeMatch inm = matchTree(parser, inPattern, inTree);
		assertTrue(inm.succeeded());
		// whitespace removed
		assertEquals("<j>inB", inm.get("condition").getText());
		assertEquals("0", inm.get("INT").getText());
	}
	
	@Test public void sumPrecedence() {
		String text = "1 * sum <i> in {} : -2 * 3";
		String pattern = "<lhs:uExpr> * <rhs:uExpr>";
		ParseTreeMatch m = matchExpression(text, pattern);
		assertTrue(m.succeeded());
		assertEquals("1", m.get("lhs").getText());
		String rhs = m.get("rhs").getText();
		// sumExpr : 'sum' condition ('do' | ':') expr ;
		String rhsPattern = "sum <condition> : <nExpr>";
		ParseTreeMatch rhsm = matchExpression(rhs, rhsPattern);
		assertTrue(rhsm.succeeded());
		// whitespace removed
		assertEquals("<i>in{}", rhsm.get("condition").getText());
		assertEquals("-2*3", rhsm.get("nExpr").getText());
	}
	
	@Test public void conditionWithNone() {
		ExpressionParser parser = createExpressionParserFor("<i> in { }");
		ParseTree tree = parser.condition();
		// condition : tuple 'in' setExpr (...)? ;
		String pattern = "<tuple> in <setExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertEquals("<i>", m.get("tuple").getText());
		assertEquals("{}", m.get("setExpr").getText()); // WS removed
	}
	
	@Test public void conditionWithBoolExpr() {
		String text = "<i> in { } with true";
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.condition();
		// condition : tuple 'in' setExpr (('with' | '|') boolExpr)? ;
		String pattern = "<tuple> in <setExpr> with <boolExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertEquals("<i>", m.get("tuple").getText());
		assertEquals("true", m.get("boolExpr").getText());
	}
	
	@Test public void conditionBarBoolExpr() {
		String text = "<i> in { } | true";
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = parser.condition();
		// condition : tuple 'in' setExpr (('with' | '|') boolExpr)? ;
		String pattern = "<tuple> in <setExpr> | <boolExpr>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertEquals("<i>", m.get("tuple").getText());
		assertEquals("true", m.get("boolExpr").getText());
	}
	
	@Test public void conditionMisspelledIn() {
		int rule = ExpressionParser.RULE_condition;
		List<String> errors = getExpressionErrorsFor("<i> at { }", rule);
		String expected = "line 1:7 mismatched input '{' expecting " +
				"{'+', '-', '*', '/', '<', '>', 'and', 'xor', 'or', " +
				"'<=', '!=', '==', '>=', '[', ','}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void conditionMisspelledWith() {
		String text = "<i> in { } wiht true";
		ExpressionParser parser = createExpressionParserFor(text);
		parser.condition(); // begin parsing at 'condition' rule
		// condition : tuple 'in' setExpr ('with' boolExpr)?;
		assertFalse(hasConsumedAllInput(parser));
	}
	
	@Test public void conditionMissingTuple() {
		int rule = ExpressionParser.RULE_condition;
		List<String> errors = getExpressionErrorsFor(" in { }", rule);
		String expected = "line 1:1 mismatched input 'in' expecting '<'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void conditionMissingKeyword() {
		int rule = ExpressionParser.RULE_condition;
		List<String> errors = getExpressionErrorsFor("<i> { }", rule);
		// whitespace removed
		String expected0 = "line 1:4 no viable alternative at input 'i>{'";
		String expected1 = "line 1:4 missing 'in' at '{'";
		assertEquals(List.of(expected0, expected1), errors);
	}
	
	@Test public void conditionMissingSetExpr() {
		int rule = ExpressionParser.RULE_condition;
		List<String> errors = getExpressionErrorsFor("<i> in", rule);
		String expected = "line 1:6 mismatched input '<EOF>' " +
				"expecting {'(', '{', 'if', ID}";
		assertEquals(List.of(expected), errors);
	}
	
	// boolExpr ------------------------------------
	
	@Test public void boolExprCondition() {
		testLabeledExpr("<i> in I", RULE_boolExpr, "condition");
	}
	
	@Test public void boolExprComparison() {
		testLabeledExpr("0 < 1", RULE_boolExpr, "comparison");
	}
	
	@Test public void boolExprAnd() {
		testLabeledExpr("true and true", RULE_boolExpr, "boolExpr");
	}
	
	@Test public void boolExprXor() {
		testLabeledExpr("true xor false", RULE_boolExpr, "boolExpr");
	}
	
	@Test public void boolExprOr() {
		testLabeledExpr("false or true", RULE_boolExpr, "boolExpr");
	}
	
	@Test public void boolExprMisspelledToken() {
		List<String> errors =
				getExpressionErrorsFor("treu", RULE_boolExpr);
		String expected = "line 1:4 no viable alternative at input 'treu'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void boolExprTrue() {
//		boolExpr: condition | comparison | 'not' boolExpr
//				| boolExpr ( 'and' | 'xor' | 'or' ) boolExpr
//				| 'true' | 'false'
		testLabeledExpr("true", RULE_boolExpr, "boolExpr");
	}
	
	@Test public void boolExprFalse() {
		testLabeledExpr("false", RULE_boolExpr, "boolExpr");
	}
	
	
	// comparison -----------------------------------------
	@Test public void comparisonLessThan() {
//		// comparison : expr cmp expr ( cmp expr ) ? ;
//		comparison: nExpr bound+ ;
//		bound	: cmp ( nExpr | sign=('+'|'-')? INFINITY )
//		cmp		: '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		String pattern = "<lhs:nExpr> <cmp> <rhs:nExpr>";
		ParseTreeMatch m = matchComparison("0 < 1", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("0", "<", "1"),
				asStrings(m, "lhs", "cmp", "rhs"));
	}
	
	@Test public void comparisonLessThanOrEqualTo() {
		String pattern = "<lhs:nExpr> <cmp> <rhs:nExpr>";
		ParseTreeMatch m = matchComparison("a <= 0", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "<=", "0"),
				asStrings(m, "lhs", "cmp", "rhs"));
	}
	
	@Test public void comparisonEqualTo() {
		String pattern = "<lhs:nExpr> <cmp> <rhs:nExpr>";
		ParseTreeMatch m = matchComparison("0 == 1", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("0", "==", "1"),
				asStrings(m, "lhs", "cmp", "rhs"));
	}
	
	@Test public void comparisonNotEqualTo() {
//		// comparison : expr cmp expr ( cmp expr ) ? ;
//		comparison: nExpr bound+ ;
//		bound	: cmp ( nExpr | sign=('+'|'-')? INFINITY )
//		cmp		: '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		String pattern = "<lhs:nExpr> <cmp> <rhs:nExpr>";
		ParseTreeMatch m = matchComparison("0 != 1", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("0", "!=", "1"),
				asStrings(m, "lhs", "cmp", "rhs"));
	}
	
	@Test public void comparisonGreaterThan() {
		String pattern = "<lhs:nExpr> <cmp> <rhs:nExpr>";
		ParseTreeMatch m = matchComparison("0 > 1", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("0", ">", "1"),
				asStrings(m, "lhs", "cmp", "rhs"));
	}
	
	@Test public void comparisonGreaterThanOrEqualTo() {
		String pattern = "<lhs:nExpr> <cmp> <rhs:nExpr>";
		ParseTreeMatch m = matchComparison("0 >= b", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("0", ">=", "b"),
				asStrings(m, "lhs", "cmp", "rhs"));
	}
	
	@Test public void comparisonMisformedLeftShift() {
//		comparison: nExpr bound+ ;
//		bound	:	cmp nExpr ;
//		cmp		: '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		List<String> errors = getComparisonErrorsFor("0 << 1");
		String expected = "line 1:3 extraneous input '<' expecting " +
				"{'+', '-', '(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void comparisonMisformedSpaceship() {
		List<String> errors = getComparisonErrorsFor("0 <=> 1");
		String expected = "line 1:4 extraneous input '>' expecting " +
				"{'+', '-', '(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void comparisonMisformedDiamond() {
		List<String> errors = getComparisonErrorsFor("0 <> 1");
		String expected = "line 1:3 extraneous input '>' expecting " +
				"{'+', '-', '(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void comparisonMisformedStrictEquality() {
		List<String> errors = getComparisonErrorsFor("0 === 1");
		String expected = "line 1:4 token recognition error at: '= '";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void comparisonMisformedStrictInequality() {
//		comparison: nExpr bound+ ;
//		bound	:	cmp ( nExpr | ('+'|'-')? 'infinity' ) ;
//		cmp		: '<' | '<=' | '!=' | '==' | '>' | '>=' ;
		List<String> errors = getComparisonErrorsFor("0 !== 1");
		String expected = "line 1:4 token recognition error at: '= '";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void comparisonMisformedAssignment() {
		List<String> errors = getComparisonErrorsFor("0 = 1");
		String expected0 = "line 1:2 token recognition error at: '= '";
		String expected1 = "line 1:4 mismatched input '1' expecting " +
				"{'<', '>', '<=', '!=', '==', '>='}";
		assertIterableEquals(List.of(expected0, expected1), errors);
	}
	
	@Test public void comparisonMisformedRightArrow() {
		List<String> errors = getComparisonErrorsFor("0 => 1");
		List<String> expected = List.of(
				"line 1:2 token recognition error at: '=>'",
				"line 1:5 mismatched input '1' " +
						"expecting {'<', '>', '<=', '!=', '==', '>='}"
			);
		assertIterableEquals(expected, errors);
	}
	
	
	// other ---------------------------------------------
	
	@Test public void sqRefCsvMany() {
		testSqRef("m[x,2]", "m", "x,2");
	}
	
	@Test public void sqRefCsvSingle() {
		testSqRef("c[0]", "c", "0");
	}
	
	@Test public void sqRefCsvEmpty() {
		testSqRef("a[]", "a", "");
	}
	
	@Test public void sqRefUnindexed() {
		ParseTreeMatch m = matchExpression("b", "<ID>", RULE_sqRef);
		assertTrue(m.succeeded());
	}
	
	@Test public void sqRefMissingIndexLast() {
		List<String> errors = getExpressionErrorsFor("d[1,]", RULE_sqRef);
		String expected = "line 1:4 mismatched input ']' expecting " +
				"{'+', '-', '(', '{', '<', 'not', 'true', 'false', 'if', " +
				"'infinity', INT, FLOAT, STRING, ID}";
		assertIterableEquals(List.of(expected), errors);
	}
	
	@Test public void sqRefMissingIndexFirst() {
		List<String> errors = getExpressionErrorsFor("e[,3]", RULE_sqRef);
		String expected = "line 1:2 no viable alternative at input 'e[,'";
		assertIterableEquals(List.of(expected), errors);
	}
	
	@Test public void sqRefMissingBracketOpening() {
		ExpressionParser parser = createExpressionParserFor("w]");
		parser.sqRef();
		assertFalse(hasConsumedAllInput(parser));
	}
	
	@Test public void sqRefMissingBracketClosing() {
		List<String> errors = getExpressionErrorsFor("w[");
		String expected = "line 1:2 no viable alternative at input 'w['";
		assertIterableEquals(List.of(expected), errors);
	}
	
	@Test public void sqRefInvalidId() {
		// (optional) square-brackets reference: params & vars
//		sqRef: name=ID ('[' csv ']')? ;
//		csv	 : ( elem (',' elem)* )? ;
		List<String> errors = getExpressionErrorsFor("?[]");
		List<String> expected = List.of(
				"line 1:0 token recognition error at: '?'",
				"line 1:1 mismatched input '[' expecting {'+', '-', " +
					"'(', 'if', 'infinity', INT, FLOAT, ID}"
			);
		assertIterableEquals(expected, errors);
	}
	
	@Test public void fnRefMany() {
		testFnRef("f(x,0)", "f", "x,0");
	}
	
	@Test public void fnRefSingle() {
		testFnRef("g(1)", "g", "1");
	}
	
	@Test public void fnRefEmpty() {
		testFnRef("h()", "h", "");
	}
	
	@Test public void fnRefInvalidArgument() {
		List<String> errors = getExpressionErrorsFor("N(v = 2)");
		String expected0 = "line 1:4 token recognition error at: '= '";
		String expected1 = "line 1:6 extraneous input '2' expecting ')'";
		assertEquals(List.of(expected0, expected1), errors);
	}
	
	@Test public void fnRefBlankArguments() {
		List<String> errors = getExpressionErrorsFor("N(,)");
		String expected = "line 1:2 extraneous input ',' expecting ')'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void fnRefMissingParenthesis() {
		List<String> errors = getExpressionErrorsFor("N(");
		String expected = "line 1:2 missing ')' at '<EOF>'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void fnRefInvalidID() {
		List<String> errors = getExpressionErrorsFor("_()");
		String expected0 = "line 1:0 token recognition error at: '_'";
		String expected1 = "line 1:2 mismatched input ')' expecting " +
				"{'+', '-', '(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected0, expected1), errors);
	}
	
	@Test public void csvSingleID() {
		testCsvSingleExpr("i", "ID");
	}
	
	@Test public void csvSingleInt() {
		testCsvSingleExpr("0", "INT");
	}
	
	@Test public void csvSingleFloat() {
		testCsvSingleExpr("1.0", "FLOAT");
	}
	
	@Test public void csvSingleString() {
		testCsvSingleExpr("\"a\"", "STRING"); // escaped "a"
	}
	
	public void testCsvSingleExpr(String text, String label) {
//		csv		:	( expr (',' expr)* )? ;
//		expr	: nExpr | strExpr | boolExpr | setExpr | tuple ;
		int rule = ExpressionParser.RULE_csv;
		testLabeledExpr(text, rule, label);
	}
	
	@Test public void exprId() {
		testExpr("A", "ID", ExpressionParser.ID);
	}
	
	@Test public void exprInt() {
		testExpr("0", "INT", ExpressionParser.INT);
	}
	
	@Test public void exprFloat() {
		testExpr("1.0", "FLOAT", ExpressionParser.FLOAT);
	}
	
	@Test public void exprString() {
		testExpr("\"s\"", "STRING", ExpressionParser.STRING);
	}
	
	@Test public void testax0() {
		ParseTreeMatch m = matchExpression("a*0", "<ID> * <INT>");
		assertTrue(m.succeeded());
		assertEquals("a", m.get("ID").getText());
		assertEquals("0", m.get("INT").getText());
	}
	
	@Test public void test1øCbƆ() {
		ParseTreeMatch m = matchExpression("1/(b)", "<INT> / ( <ID> )");
		assertTrue(m.succeeded());
		assertEquals("1", m.get("INT").getText());
		assertEquals("b", m.get("ID").getText());
	}
	
	@Test public void testC0Ɔ() {
		ParseTreeMatch m = matchExpression("(0)", "( <nExpr> )");
		assertTrue(m.succeeded());
		assertEquals("0", m.get("nExpr").getText());
	}
	
	@Test public void testC0ᵢ0Ɔ() {
		ParseTreeMatch m = matchExpression("(0.0)", "( <nExpr> )");
		assertTrue(m.succeeded());
		assertEquals("0.0", m.get("nExpr").getText());
	}
	
	@Test public void testCaƆ() {
		ParseTreeMatch m = matchExpression("(a)", "( <nExpr> )");
		assertTrue(m.succeeded());
		assertEquals("a", m.get("nExpr").getText());
	}
	
	@Test public void testEmptyExpression() {
		List<String> errors = getExpressionErrorsFor("");
		// WARN: vscode replaces '<>' with '[]' in comparison dialogs
		// from assertion failures; makes problem detection hard. 
		String expected = "line 1:0 mismatched input '<EOF>' expecting " +
				"{'+', '-', '(', 'if', 'infinity', INT, FLOAT, ID}";
		assertEquals(List.of(expected), errors);
	}
	
}
