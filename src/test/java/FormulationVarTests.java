import static org.junit.jupiter.api.Assertions.*;
import static util.FormulationTesting.*;
import static util.CommonTesting.*;

import java.util.List;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;
import org.antlr.v4.runtime.tree.pattern.ParseTreePatternMatcher.StartRuleDoesNotConsumeFullPattern;
import org.junit.Test;

import de.zib.zimpl.antlr.FormulationParser;

public class FormulationVarTests {
	
	@Test public void varMinimal() {
		FormulationParser parser = createFormulationParserFor("var a");
		ParseTree tree = parser.variable();
		ParseTreeMatch m = matchTree(parser, "var <ID>", tree);
		assertTrue(m.succeeded());
		assertEquals("a", m.get("ID").getText());
	}
	
	@Test public void varIndexedById() {
		String text = "var a[E]";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.variable();
//		variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
//		sqRef: name=ID ('[' (csv | index) ']')? ;
		String pattern = "var <name:ID>[<index:ID>]";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "E"),
				asStrings(m, "name", "index"));
	}
	
	@Test public void varIndexedByCondition() {
		String text = "var a[<i> in {}]";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.variable();
		String pattern = "var <ID>[<condition>]";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "<i>in{}"), // WS removed
				asStrings(m, "ID", "condition"));
	}
	
	@Test public void varTypedBinary() {
		FormulationParser parser = createFormulationParserFor("var b binary");
		ParseTree tree = parser.variable();
		ParseTreeMatch m = matchTree(parser, "var <ID> <VAR_TYPE>", tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("b", "binary"),
				asStrings(m, "ID", "VAR_TYPE"));
	}
	
	@Test public void varTypedInteger() {
		FormulationParser parser = createFormulationParserFor("var i integer");
		ParseTree tree = parser.variable();
//		variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
//		sqRef: name=ID ('[' (csv | index) ']')? ;
		ParseTreeMatch m = matchTree(parser, "var <ID> <VAR_TYPE>", tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("i", "integer"),
				asStrings(m, "ID", "VAR_TYPE"));
	}
	
	@Test public void varTypedReal() {
		FormulationParser parser = createFormulationParserFor("var r real");
		ParseTree tree = parser.variable();
		ParseTreeMatch m = matchTree(parser, "var <ID> <VAR_TYPE>", tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("r", "real"),
				asStrings(m, "ID", "VAR_TYPE"));
	}
	
	@Test public void varTypedUnknown() {
		String text = "var a unkwnownType";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.variable();
		String pattern = "var <ID> <VAR_TYPE>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertFalse(m.succeeded());
	}
	
	@Test public void varTypedUnknownAsStatement() {
//		variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
//		sqRef: name=ID ('[' (csv | index) ']')? ;
		List<String> errors = getFormulationErrorsFor("var a unkwnownType;");
		String expected = "line 1:6 extraneous input 'unkwnownType' " +
				"expecting ';'";
		assertEquals(List.of(expected), errors);
	}
	
	@Test public void varBoundsSingleValue() {
		String pattern = "var <ID> <bound>";
		ParseTreeMatch m = matchVariable("var a<=+10", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "<=+10"), // whitespace removed
				asStrings(m, "ID", "bound"));
	}
	
	@Test public void varBoundsDoubleValue() {
		String pattern = "var <ID> <b1:bound> <b2:bound>";
		ParseTreeMatch m = matchVariable("var a >= 0 <= 1", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", ">=0", "<=1"), // WS removed
				asStrings(m, "ID", "b1", "b2"));
	}
	
	@Test public void varBoundsSingleInfinity() {
//		variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
//		sqRef: name=ID ('[' (csv | index) ']')? ;
		String pattern = "var <ID> <bound>";
		ParseTreeMatch m = matchVariable("var a <= infinity", pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "<=infinity"), // WS removed
				asStrings(m, "ID", "bound"));
	}
	
	@Test public void varBoundsDoubleSignedInfinity() {
		String text = "var a <= +infinity >= -infinity";
		String pattern = "var <ID> <b1:bound> <b2:bound>";
		ParseTreeMatch m = matchVariable(text, pattern);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("a", "<=+infinity", ">=-infinity"),
				asStrings(m, "ID", "b1", "b2")); // WS removed
	}
	
	@Test public void varBoundsTripleValue() {
		String text = "var a >= 1 == 2 <= 3";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.variable();
//		variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
//		sqRef: name=ID ('[' (csv | index) ']')? ;
		String pattern = "var <ID> <b1:bound> <b2:bound> <b3:bound>";
		assertThrows(StartRuleDoesNotConsumeFullPattern.class, () -> {
				matchTree(parser, pattern, tree);
			});
	}
	
	@Test public void varBoundsTripleValueAsStatement() {
		String text = "var a >= 1 == 2 <= 3 ;";
		FormulationParser parser = createFormulationParserFor(text);
		ParseTree tree = parser.statement();
		// statement : ( variable | ... ) ';'
		String pattern = "<variable> ;";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertFalse(m.succeeded());
	}
	
	@Test public void varMany() {
		FormulationParser parser = createFormulationParserFor("var a; var b;");
		ParseTree tree = parser.formulation();
		// formulation : statement*
		String pattern = "<s1:statement> <s2:statement>";
		ParseTreeMatch m = matchTree(parser, pattern, tree);
		assertTrue(m.succeeded());
		assertIterableEquals(List.of("vara;", "varb;"), // WS removed
				asStrings(m, "s1", "s2"));
	}
	
}
