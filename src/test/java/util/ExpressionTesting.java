package util;

import static org.junit.jupiter.api.Assertions.*;
import static zimpl.ZimplParser.setupErrorListenersFor;
import static de.zib.zimpl.antlr.ExpressionParser.*;
import static util.CommonTesting.*;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;

import de.zib.zimpl.antlr.ExpressionLexer;
import de.zib.zimpl.antlr.ExpressionParser;
import zimpl.Parsers;

import java.util.List;

public class ExpressionTesting {
	
	public static ExpressionParser createExpressionParserFor(String text) {
		return createExpressionParserFor(text, false);
	}
	
	public static ExpressionParser
	createExpressionParserFor(String text, boolean attachDiagnostic) {
		// create a CharStream that reads from the String parameter
		CharStream input = CharStreams.fromString(text);
		// create a lexer that feeds off of input CharStream
		ExpressionLexer lexer = new ExpressionLexer(input);
		// create a buffer of tokens pulled from the lexer
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		// create a parser that feeds off the tokens buffer
		ExpressionParser parser = new ExpressionParser(tokens);
		setupErrorListenersFor(List.of(lexer, parser), attachDiagnostic);
		return parser;
	}
	
	public static List<String> getComparisonErrorsFor(String text) {
		return getExpressionErrorsFor(text, RULE_comparison);
	}
	
	public static List<String> getExpressionErrorsFor(String text) {
		// FIXME: shouldn't rule refer to "general" expressions
		// (including boolean, strings,...) ? Yes, it should; but beware
		// of general "no viable alternative" instead of informative
		// "expecting {...}".
		return getExpressionErrorsFor(text, RULE_nExpr);
	}
	
	public static List<String> getExpressionErrorsFor(String text, int rule) {
		ExpressionParser parser = createExpressionParserFor(text);
		Parsers.applyRule(parser, rule);
		return Parsers.getErrors(parser);
	}
	
	public static void diagnoseExpression(String text) {
		// FIXME: shouldn't rule refer to "general" expressions
		// (including boolean, strings,...) ? Yes, it should; but beware
		// of general "no viable alternative" instead of informative
		// "expecting {...}".
		diagnoseExpressionAs(text, RULE_nExpr);
	}
	
	public static void diagnoseExpressionAs(String text, int rule) {
		List<String> errors = getExpressionErrorsFor(text, rule);
		System.out.println("Text: '" + text + "'.");
		String errorsMsg = errors.isEmpty() ? "No errors found" :
				"Errors: " + errors;
		System.out.println(errorsMsg);
		ExpressionParser parser = createExpressionParserFor(text);
		parser.setTrace(true);
		System.out.println("Parse tracing ON");
		ParseTree tree = Parsers.applyRule(parser, rule);
		System.out.println("#syntaxErr: " + parser.getNumberOfSyntaxErrors());
		// TODO: find out if there are any ErrorNodes in tree
		System.out.println("tree.root.class: " + tree.getClass().getSimpleName());
		System.out.println("tree.text: " + tree.getText());
		System.out.println("tree: " + tree.toStringTree(parser));
		if (hasConsumedAllInput(parser))
			System.out.println("parser has consumed ALL input");
		else
			System.out.println("parser has NOT consumed all input");
		
		Vocabulary vocabulary = parser.getVocabulary();
		TokenStream ts = parser.getTokenStream();
		System.out.println("tokenstream.text: " + ts.getText());
		System.out.println("consumed tokens:");
		for (int i = 0; i < ts.size(); ++i) {
			Token t = ts.get(i);
			System.out.println(formatToken(t, vocabulary));
		}
		// With text ("a<=1") this code returns the same as using parser.getTokenStream()
		// ts = parser.getInputStream();
		// System.out.println("inputstream.text: " + ts.getText());
		// System.out.println("input tokens:");
		// for (int i = 0; i < ts.size(); ++i) {
		//	 Token t = ts.get(i);
		//	 System.out.println(formatToken(t, vocabulary));
		// }
		
		// Token consumed = parser.consume(); // throws NPE
		System.out.println("DFA:");
		parser.dumpDFA();
		// ATN atn = parser.getATN();
		// System.out.println("ATN: " + atn); // ATN: org.antlr.v4.runtime.atn.ATN@f074a0c8
		// Token cToken = parser.getCurrentToken(); // last token ok, not next unconsumed
		// System.out.println("current token: " + formatToken(cToken, vocabulary));
		// System.out.println("DFA.strings: " + parser.getDFAStrings());
		// IntervalSet is = parser.getExpectedTokens(); // java.lang.IllegalArgumentException: Invalid state number.
		// IntervalSet is = parser.getExpectedTokensWithinCurrentRule(); // java.lang.IndexOutOfBoundsException: Index -1 out of bounds for length 262
		// System.out.println("intervalset: " + is.toString(vocabulary));
		// System.out.println("serial atn: " + parser.getSerializedATN()); // non-printable!
		System.out.println("parser.state: " + parser.getState());
		// parser.isExpectedToken(int symbol)...
		// parser... see ErrorStrategy
		// parser.notifyErrorListeners(null, text, null);
	}
	
	public static Token getParsedToken(String text) {
		ExpressionParser parser = createExpressionParserFor(text);
		// begin parsing at 'expr' rule
		// FIXME: shouldn't method refer to "general" expressions
		// (including boolean, strings,...) ?
//		ParseTree tree = parser.expr();
		ParseTree tree = parser.nExpr();
		return extractToken(tree);
	}
	
	/**
	 * basicExpr : ID ('[' csv ']')? | INT | FLOAT 
	 *		| sumExpression  | '(' expr ')' ;
	 */
	public static void testBasicExpr(String text) {
		testLabeledBasicExpr(text, "basicExpr");
	}
	
	public static void
	testLabeledBasicExpr(String text, String label) {
		testLabeledExpr(text, RULE_basicExpr, label);
	}
	
	public static void
	testLabeledExpr(String text, int rule, String label) {
		String pattern = "<" + label + ">";
		ParseTreeMatch m = matchExpression(text, pattern, rule);
		assertTrue(m.succeeded());
		// Whitespace removed from 'text'
		String wrText = text.replaceAll("\\s", "");
		assertEquals(wrText, m.get(label).getText());
	}
	
	public static void
	testExpr(String text, String label, int tokenType) {
//		expr	: nExpr | strExpr | boolExpr | setExpr | tuple ;
		int rule = ExpressionParser.RULE_expr;
		testLabeledExpr(text, rule, label);
		Token tok = getParsedToken(text);
		assertEquals(tokenType, tok.getType());
		assertEquals(text, tok.getText());
	}
	
	public static void
	testSqRef(String text, String id, String csv) {
		ParseTreeMatch m =
				matchExpression(text, "<ID> [ <csv> ]", RULE_sqRef);
		assertTrue(m.succeeded());
		List<String> result = asStrings(m, "ID", "csv");
		assertIterableEquals(List.of(id, csv), result);
	}
	
	public static void
	testFnRef(String text, String id, String arguments) {
		ParseTreeMatch m = 
				matchExpression(text, "<ID> ( <csv> )", RULE_fnRef);
		assertTrue(m.succeeded());
		assertEquals(id, m.get("ID").getText());
		assertEquals(arguments, m.get("csv").getText());
	}
	
	public static ParseTreeMatch
	matchExpression(String text, String pattern, int rule) {
		ExpressionParser parser = createExpressionParserFor(text);
		ParseTree tree = Parsers.applyRule(parser, rule);
		return matchTree(parser, pattern, tree);
	}
	
	public static ParseTreeMatch
	matchExpression(String text, String pattern) {
		return matchExpression(text, pattern, RULE_expr);
	}
	
	public static ParseTreeMatch
	matchSetExpr(String text, String pattern) {
		return matchExpression(text, pattern, RULE_setExpr);
	}
	
	public static ParseTreeMatch
	matchTuple(String text, String pattern) {
		return matchExpression(text, pattern, RULE_tuple);
	}
	
	public static ParseTreeMatch
	matchComparison(String text, String pattern) {
		return matchExpression(text, pattern, RULE_comparison);
	}
	
	public static ParseTreeMatch
	matchFnRef(String text, String pattern) {
		return matchExpression(text, pattern, RULE_fnRef);
	}

}
