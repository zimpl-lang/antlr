package util;

import java.util.Arrays;
import java.util.List;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;

import de.zib.zimpl.antlr.ExpressionLexer;
import zimpl.Parsers;

/** 
 * This class hosts static methods useful for Test classes.
 */
public class CommonTesting {
	
	public static boolean hasConsumedAllInput(Parser parser) {
		return parser.getCurrentToken().getType() == Token.EOF;
	}
	
	public static
	ParseTreeMatch matchTree(Parser p, String pattern, ParseTree t) {
		int rule = getRootRuleIndex(t);
		// TODO: use this code with a flag?
//		ParseTreeMatch result =
//				p.compileParseTreePattern(pattern, rule).match(t);
//		if (!result.succeeded()) {
//			String msg = "tree.toStringTree(): " + t.toStringTree(p);
//			throw new RuntimeException(msg);
//		}
//		return result;
		return p.compileParseTreePattern(pattern, rule).match(t);
	}
	
	private static int getRootRuleIndex(ParseTree t) {
		assert t instanceof RuleContext;
		// #labels don't define new rules, only new Contexts that
		// inherit from their rule.
		int result = ((RuleContext)t).getRuleContext().getRuleIndex();
		return result;
	}
	
	public static Token extractToken(ParseTree tree) {
		ParseTree current = tree;
		while (current.getChildCount() == 1)
			current = current.getChild(0);
		
		assert(current.getChildCount() == 0);
		// could be ErrorNodeImpl
		// assert(current.getClass() == TerminalNodeImpl.class);
		// assert current.getClass() instanceof TerminalNode;
		// assert TerminalNode.class.isAssignableFrom(current.getClass());
		assert current instanceof TerminalNode;
		return ((TerminalNode) current).getSymbol();
	}
	
	public static List<String>
	asStrings(ParseTreeMatch m, String... labels) {
		String[] result = new String[labels.length];
		for (int i = 0; i < labels.length; ++i)
			result[i] = m.get(labels[i]).getText();
		return Arrays.asList(result);
	}
	
	public static String asString(ParseTreeMatch m, String label) {
		return m.get(label).getText();
	}
	
	public static void diagnoseTreeMatch(Parser p, String pattern, ParseTree t) {
		ParseTreeMatch m = matchTree(p, pattern, t);
		if (m.succeeded()) {
			System.out.println("Match!");
			return;
		}
		List<String> errors = Parsers.getErrors(p);
		System.out.println("errors: " + errors);
		System.out.println("tree.text: " + t.getText()); 
		System.out.println("root: " + Trees.getNodeText(t, p)); // node.text: comparison
		System.out.println("tree: " + t.toStringTree(p));
		ParseTree miss = m.getMismatchedNode();
		System.out.println("miss.text: " + miss.getText()); // '<'
		System.out.println("miss.class: " + miss.getClass()); // ErrorNodeImpl
		// should check if 'miss' represents a single token.
		Token token = extractToken(miss);
		System.out.println("token: " + formatToken(token, p.getVocabulary()));
		ANTLRErrorStrategy es = p.getErrorHandler();
		System.out.println("recovery-mode: " + es.inErrorRecoveryMode(p));
	}
	
	public static String formatToken(Token t, Vocabulary v) {
		int index = t.getTokenIndex();
		String text = t.getText();
		int type = t.getType();
		String name = v.getDisplayName(type);
		return String.format("@%d='%s',<%d:%s>", index, text, type, name);
	}
	
	public static String getChannelText(String text, int channel) {
		CharStream input = CharStreams.fromString(text);
		ExpressionLexer lexer = new ExpressionLexer(input);
		
		List<Token> tokens = lexer.getAllTokens().stream()
				.map(Token.class::cast)
				.filter(token -> channel == token.getChannel())
				.toList();
		
		return String.join(",", tokens.stream().map(Token::getText).toList());
	}
}
