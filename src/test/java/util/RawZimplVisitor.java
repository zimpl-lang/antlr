package util;

import java.io.IOException;

import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import de.zib.zimpl.antlr.FormulationBaseVisitor;
import de.zib.zimpl.antlr.FormulationParser.CommandContext;
import de.zib.zimpl.antlr.FormulationParser.StatementContext;
import zimpl.ZimplParser;
import de.zib.zimpl.antlr.FormulationVisitor;

public class RawZimplVisitor extends FormulationBaseVisitor<String>
implements FormulationVisitor<String>
{
	public static String fromFilename(String filename) throws IOException {
		ZimplParser parser = ZimplParser.fromFilename(filename);
		ParseTree tree = parser.parse();
		return tree.accept(new RawZimplVisitor());
	}
	
	@Override public String visitTerminal(TerminalNode node) {
		return node.getText();
	}
	
	@Override public String visitCommand(CommandContext ctx) {
		return visitChildren(ctx) + "\n";
	}
	
	@Override public String visitStatement(StatementContext ctx) {
		return visitChildren(ctx) + "\n";
	}
	
	protected String aggregateResult(String aggregate, String nextResult) {
		if (nextResult == null)
			return aggregate;
		if (aggregate == null)
			return nextResult;
		String sep = (aggregate.isBlank() || aggregate.endsWith("\n"))?
				"" : " ";
		return aggregate + sep + nextResult; 
	}
	
}
