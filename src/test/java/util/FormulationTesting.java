package util;

import static util.CommonTesting.*;
import static zimpl.ZimplParser.*;

import java.io.IOException;
import java.util.List;

import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.pattern.ParseTreeMatch;

import zimpl.ZimplParser;

public class FormulationTesting {
	
	/**
	 * Parses text, beginning at 'formulation' rule.
	 * @param text to parse
	 */
	public static ParseTree parseFormulation(String text) {
		return fromString(text).formulation();
	}
	
	public static ZimplParser createFormulationParserFor(String text) {
		return fromString(text);
	}
	
	public static List<String>
	getParseErrorsFor(String filename) throws IOException {
		ZimplParser parser = fromFilename(filename);
		parser.parse();
		return parser.getErrors();
	}
	
	public static List<String> getFormulationErrorsFor(String text) {
		return getErrorsFor(text, RULE_formulation);
	}
	
	public static List<String> getFunctionErrorsFor(String text) {
		return getErrorsFor(text, RULE_function);
	}
	
	public static List<String> getErrorsFor(String text, int rule) {
		ZimplParser parser = fromString(text);
		parser.applyRule(rule);
		return parser.getErrors();
	}
	
	public static void diagnoseFormulation(String text) {
		diagnoseFormulationAs(text, RULE_formulation);
	}
	
	public static void diagnoseFormulationAs(String text, int rule) {
		List<String> errors = getErrorsFor(text, rule);
		if (!errors.isEmpty()) {
			System.err.println("Found errors: " + errors);
			return;
		}
		System.out.println("No errors founds in text: " + text);
		ZimplParser parser = fromString(text);
		parser.setTrace(true);
		System.out.println("Parse tracing ON");
		ParseTree tree = parser.applyRule(rule);
		System.out.println("#syntaxErr: " + parser.getNumberOfSyntaxErrors());
		// TODO: find out if there are any ErrorNodes in tree
		assert parser.getNumberOfSyntaxErrors() == 0;
		
		System.out.println("tree.text: " + tree.getText());
		System.out.println("tree: " + tree.toStringTree(parser));
		
		if (hasConsumedAllInput(parser))
			System.out.println("parser has consumed ALL input");
		else
			System.out.println("parser has NOT consumed all input");
		
		Vocabulary vocabulary = parser.getVocabulary();
		TokenStream ts = parser.getTokenStream();
		System.out.println("tokenstream.text: " + ts.getText());
		System.out.println("consumed tokens:");
		for (int i = 0; i < ts.size(); ++i) {
			Token t = ts.get(i);
			System.out.println(formatToken(t, vocabulary));
		}
		// With text ("a<=1") this code returns the same as using parser.getTokenStream()
		// ts = parser.getInputStream();
		// System.out.println("inputstream.text: " + ts.getText());
		// System.out.println("input tokens:");
		// for (int i = 0; i < ts.size(); ++i) {
		//	 Token t = ts.get(i);
		//	 System.out.println(formatToken(t, vocabulary));
		// }
		
		// Token consumed = parser.consume(); // throws NPE
		System.out.println("DFA:");
		parser.dumpDFA();
		// ATN atn = parser.getATN();
		// System.out.println("ATN: " + atn); // ATN: org.antlr.v4.runtime.atn.ATN@f074a0c8
		// Token cToken = parser.getCurrentToken(); // last token ok, not next unconsumed
		// System.out.println("current token: " + formatToken(cToken, vocabulary));
		// System.out.println("DFA.strings: " + parser.getDFAStrings());
		// IntervalSet is = parser.getExpectedTokens(); // java.lang.IllegalArgumentException: Invalid state number.
		// IntervalSet is = parser.getExpectedTokensWithinCurrentRule(); // java.lang.IndexOutOfBoundsException: Index -1 out of bounds for length 262
		// System.out.println("intervalset: " + is.toString(vocabulary));
		// System.out.println("serial atn: " + parser.getSerializedATN()); // non-printable!
		System.out.println("parser.state: " + parser.getState());
		// parser.isExpectedToken(int symbol)...
		// parser... see ErrorStrategy
		parser.notifyErrorListeners(null, text, null);
		
	}
	
	public static ParseTreeMatch
	matchFunction(String text, String pattern) {
		return matchFormulation(text, pattern, RULE_function);
	}
	
	public static ParseTreeMatch
	matchConstraint(String text, String pattern) {
		return matchFormulation(text, pattern, RULE_constraint);
	}
	
	public static ParseTreeMatch
	matchVariable(String text, String pattern) {
		return matchFormulation(text, pattern, RULE_variable);
	}
	
	public static ParseTreeMatch 
	matchParameter(String text, String pattern) {
		return matchFormulation(text, pattern, RULE_parameter);
	}
	
	public static ParseTreeMatch 
	matchSet(String text, String pattern) {
		return matchFormulation(text, pattern, RULE_set);
	}
	
	public static ParseTreeMatch 
	matchFormulation(String text, String pattern) {
		return matchFormulation(text, pattern, RULE_formulation);
	}
	
	public static ParseTreeMatch 
	matchFormulation(String text, String pattern, int rule) {
		ZimplParser parser = fromString(text);
		ParseTree tree = parser.applyRule(rule);
		ParseTreeMatch result = matchTree(parser, pattern, tree);
		if (result.succeeded() && !parser.getErrors().isEmpty()) {
			String msg = "Unexpected parser errors found" + "\n" +
					"errors: " + parser.getErrors().toString() + "\n" +
					"text: '" + text + "'\n" +
					"parse rule: " + parser.getRuleNames()[rule] + "\n" +
					"tree: " + tree.toStringTree(parser);
			throw new RuntimeException(msg);
		}
		return result;
	}
	
}
