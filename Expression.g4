/**
 * ANTLR grammar for Expressions in the Zimpl language. <p/>
 *
 * @see <a href="https://zimpl.zib.de/download/zimpl.pdf">
 * 		[Koch2020] Zimpl User Guide </a>
 * @see <a href="https://www.antlr.org/">ANTLR</a>
 * @see <a href="https://dl.acm.org/doi/10.5555/1823613"> [Parr2009]
 * 		Language Implementation Patterns. ISBN 193435645X.</a>
 */
grammar Expression;

import LexerRules;

/**
 * "Imaginary" (AST) tokens, as described in [Parr2009, p78]. They
 * help represent language constructs.
 */
tokens { PAREN, SET_EMPTY }

/** numerical expression. [Koch2020] calls this a Term. */
// TODO: add tests for sign!
nExpr:	sign=('+'|'-')? uExpr ;

/** unsigned (numerical) expression */
uExpr
	:	uExpr op=('*'|'/') uExpr
	|	uExpr op=('+'|'-') uExpr
	|	basicExpr
	;

// TODO: add tests for BasicExprIf
basicExpr
	:	token=(INT|FLOAT|INFINITY)	# BasicExprToken
	|	(fnRef | sqRef | redExpr)	# BasicExprStack
	|	'(' nExpr ')'				# BasicExprParen
	|	ifExpr						# BasicExprIf
	;

// TODO: add tests for ifExpr
setExpr
	:	setExpr op='*' setExpr				# SetExprBin
	|	setExpr op=('+'|'\\'|'-') setExpr	# SetExprBin
	|	'(' setExpr ')'						# SetExprParen
	|	(setDesc | fnRef | sqRef | ifExpr)	# SetExprStack
	;

setDesc:'{' '}'						# SetDescEmpty
	|	'{' csv '}'					# SetDescStack
	|	'{' range '}'				# SetDescStack 
	|	'{' condition '}' 			# SetDescStack
	;

range	:	lhs=nExpr '..' rhs=nExpr ;

tuple	:	'<' csv '>' ;

/* reduce expression */
// TODO: test non-sum
//sumExpr :	'sum' condition sep=('do' | ':') nExpr ;
//redExpr :	op=('min'|'max'|'prod'|'sum') condition sep=('do'|':') nExpr ;
// Defining RED as lexical rule brings token conflict with (functions) 'min', 'max'
redExpr :	op=ID condition sep=('do'|':') nExpr ;

index	:	condition | setExpr ;

// In Section 4.5, [Koch2020] calls this an 'index'.
condition : tuple 'in' setExpr (sep=('with' | '|') boolExpr)? ;
// membership? 'condition' seems a synonym of boolExpr, not a subtype of it

/** boolean expression */
// TODO: add more tests
boolExpr
	:	( condition | comparison )		# BoolExprStack
	|	'not' boolExpr					# BoolExprNot
	|	boolExpr op='and' boolExpr		# BoolExprBin
	|	boolExpr op='xor' boolExpr		# BoolExprBin
	|	boolExpr op='or' boolExpr		# BoolExprBin
	|	token=('true'|'false')			# BoolExprToken
	|	'(' boolExpr ')'				# BoolExprParen
	|	ifExpr							# BoolExprStack
	;

// TODO: add 'bound+' & strExpr tests
comparison
	:	nExpr bound+					# ComparisonNExpr
	|	lhs=strExpr cmp rhs=strExpr		# ComparisonStrExpr
	;

bound	:	cmp nExpr ;
// Defining CMP as lexical rule brings token conflict with <tuple>.
cmp		:	token=( '<' | '<=' | '!=' | '==' | '>=' | '>' ) ;
// Lex rule INFINITY generates associated Token type, convenient for AST
INFINITY:	'infinity' ;

// reference with (optional) square-brackets: indexed sets, params & vars
sqRef	:	name=ID ('[' csv ']')?	# SqRefCsv		// accepts 'id[]'
		|	name=ID '[' index ']'		# SqRefIndex
		;

fnRef	:	name=ID '(' csv ')' ;

// TODO: try using 'csv' for any comma separated list: indices, chains...
// Then, delegate to a future type checker visitor the enforcement of
// zimpl-lang compatibilitiy issues (which forbids some combinations,
// such as tuples of tuples ("< <a>, b>") or sets as function args.

csv		:	( expr (',' expr)* )? ;

duo 	:	tuple expr ;

// NOTE: 'duo' is excluded from this list as it otherwise leads to unin-
// tended parse trees in 'param', where a single 'duo' is a proper expr,
// instead of an element in a chain of mappings, and also in 'condition'
// TODO: add lots of tests
expr	:	nExpr | strExpr | boolExpr | setExpr | tuple ;

// TODO: add tests!
strExpr	:	STRING					# StrExprToken
		|	(fnRef | sqRef)			# StrExprStack
		|	strExpr op='+' strExpr	# StrExprBin
		|	ifExpr					# StrExprIf
		;

ifExpr	:	'if' boolExpr 'then' thenExpr=expr 'else' elseExpr=expr 'end' ;
