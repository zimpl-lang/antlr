/**
 * ANTLR grammar for the Zimpl language. <p/>
 *
 * This grammar allows for the distinction between (abstract)
 * Formulations, where some elements may be declared but not assigned
 * any value, and Models, where all elements are concretely defined.
 * 
 * @see <a href="https://zimpl.zib.de/download/zimpl.pdf">
 * 		[Koch2020] Zimpl User Guide </a>
 * @see <a href="https://www.antlr.org/">ANTLR</a>
 * @see <a href="https://dl.acm.org/doi/10.5555/1823613"> [Parr2009]
 * 		Language Implementation Patterns. ISBN 193435645X.</a>
 */
grammar Formulation;

import Expression;


/** Start rule. */
parse:	formulation EOF ;

formulation : (command | statement)* ;

// TODO: test commands
command : 'do' forall* ( print | check ) ';' ;

check	: 'check' boolExpr ;

print	: 'print' csv ;

statement :
	(	set
	|	parameter
	|	variable
	|	objective
	|	constraint
	|	function
	)	';'
	;

// NOTE [design guideline]: it's generally a good idea to name rule
// fields but excessive naming bites readability; prefer naming if AST
// construction benefits from it. Eg: adding named field 'value' in
// 		set : 'set' name=ID ':=' value=setExpr	# SetDefExpr | ...
// is clear enough but does not benefit AST construction since 'setExpr'
// is readily and unambiguously available.
// Contrast this with
// 		set : 'set' name=ID					# SetDecl
// where named field 'name' improves readability AND is used in AST
// construction to extract Token from lex rule.

// TODO: indexed sets are not fully tested
set :	'set' sqRef							# SetDecl
	|	'set' sqRef ':=' setExpr			# SetDefExpr
	|	'set' sqRef ':=' '{' readfile '}'	# SetDefRead
	;

// NOTE [design guideline]: consistency is very important but sometimes
// leads to tedious repetition; prefer consistency unless AST
// construction suffers from it. Eg: defining 'parameter' subrules
// 		: 'param' sqRef						# ParamDecl
// 		| 'param' sqRef ':=' elem			# ParamDefElem ...
// is consistent with current 'set' rule def but leads to almost
// identical duplicated methods exitParamDecl() & exitParamDefElem()
// in AST construction.

parameter
	:	'param' sqRef ( ':=' expr )?		# ParamDecl
	|	'param' sqRef ':=' mapping			# ParamDef
	;

// FIXME: there's another possible 'index' & 'mapping' combination,
// using expressions such as:
//		'param x[<i> in { 1 .. 8 } with i mod 2 == 0] := 3 * i ;'

// TODO: add 'mapping' ('readfile', 'default', empty chain, 'elem'?) tests
// FIXME: visitors get ugly; should label alternatives?
mapping : ( readfile | chain | readfile ',' chain) ('default' expr)? ;

// FIXME: should empty chains be allowed? They are not in Zimpl. When
// combined with 'default' they seem single elem def; otherwise, though
// mathematically possible, it's not clear how to handle them. Review!
chain	: ( duo (',' duo)* )? ;

// TODO: add 'readfile' tests
readfile: 'read' filename=STRING 'as' template=STRING readopts*;
readopts
	:	'skip' skip=INT
	|	'use' use=INT
	|	'match' match=STRING
	|	'comment' comment=STRING
	;

variable: 'var' sqRef VAR_TYPE? b1=bound? b2=bound? ;
VAR_TYPE: 'real' | 'binary' | 'integer' ;

objective: GOAL name=ID? ':' nExpr;
GOAL	: 'minimize' | 'maximize' ;

// TODO: redo tests
// Mmmm... does empty tail recursion work properly in antlr4 ?
// constraint: 'subto' name=ID? ':' forall comparison ; // nested alt (tail recursion)
constraint: 'subto' name=ID? ':' forall* comparison ;

// TODO: redo tests
// Mmmm... does empty tail recursion work properly in antlr4 ?
//forall	: 'forall' condition sep=('do' | ':') forall	# ForallNested
//		|													# ForallEmpty
//		;
forall	: 'forall' condition sep=('do' | ':') ;

function: FN_TYPE fnRef (':=' expr)? ;
FN_TYPE : 'defnumb' | 'defstrg' | 'defbool' | 'defset' ;

