lexer grammar LexerRules;

// Numbers
//---------------------------------------------------------
INT : [0-9]+ ;
FLOAT
	:	[0-9]* '.' INT Exponent?
	|	INT Exponent
	;

fragment Exponent :	ExponentIndicator Sign? [0-9]+ ;
fragment ExponentIndicator : [eE] ;
fragment Sign :	[+-] ;

// Text
//---------------------------------------------------------
STRING	:	'"' .*? '"' ;

// TODO: add tests
ID	:	StartChar IdChar* ;

fragment StartChar
	:	[a-zA-Z]
	|	[α-ωΑ-Ω]
	;
fragment IdChar
	:	StartChar
	|	[0-9]
	|	'_'
	;

// Whitespace
//---------------------------------------------------------
WS	:	[ \t\r\n]+ -> skip ;

// Comments
//---------------------------------------------------------
COMMENT_LINE : '#' .*? ('\r'? '\n' | EOF) -> channel(HIDDEN);
